package org.hshk.message.storage;

import org.hshk.message.service.CpMsgTemplateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * 2017年02月24日
 * 21:39
 * Create By WangYong's Intellij IDEA
 */
@Component
public class PersistenceMemeryContent {
    //日志记录类
    private static final Logger log = LoggerFactory.getLogger(PersistenceMemeryContent.class);
    //模版持久层
    @Autowired
    private CpMsgTemplateService templateService;
    //内存实例
    public static final Map<String, Object> contentPersistenceMap = new HashMap<String, Object>();

    /**
     * 2017年2月24日 21:44:18
     * 直接从内存中获取数据：
     * 1.如果内存中存在该条短信数据，那么直接返回
     * 2.如果内存中不存在该条短信数据，那么需要查询数据库后再进行返回
     *
     * @param mapKey
     * @return
     */
    public synchronized String getMsgContent(String mapKey) {
        //内存中没有自定义模板
        if (null == contentPersistenceMap.get(mapKey)) {
            String[] paramArray = mapKey.split("%");
            String brandId = paramArray[0];
            String templateCode = paramArray[1];
            //去数据库查该品牌自定义模板
            String result = templateService.findByUidAndTemplateCode(Integer.parseInt(brandId), templateCode);
            //数据库中无该品牌自定义模板
            if ("" == result) {
                //从内存中取品牌为999999的模板
                if (null == contentPersistenceMap.get("99999999%" + templateCode)) {
                    //如果没有，去数据库中取品牌为0的模板
                    result = templateService.findByUidAndTemplateCode(99999999, templateCode);
                    if ("" != result) {
                        contentPersistenceMap.put("99999999%" + templateCode, result);
                    } else {
                        log.info("缺少通用模板，品牌id为99999999，模板编号为：" + templateCode);
                    }
                } else {
                    result = templateService.findByUidAndTemplateCode(99999999, templateCode);
                }
            } else {
                contentPersistenceMap.put(mapKey, result);
            }
            return result;
        } else {
            return contentPersistenceMap.get(mapKey).toString();
        }
    }
}