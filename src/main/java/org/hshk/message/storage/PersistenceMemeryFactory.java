package org.hshk.message.storage;

import org.hshk.message.entity.CpMsgMemberSign;
import org.hshk.message.repository.CpMsgMemberSignRepository;
import org.hshk.message.service.CpMsgFactoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 2017年02月24日
 * 21:39
 * Create By WangYong's Intellij IDEA
 */
@Component
public class PersistenceMemeryFactory
{
    //日志记录类
    private static final Logger log = LoggerFactory.getLogger(PersistenceMemeryFactory.class);
    @Autowired
    private CpMsgMemberSignRepository cpMsgMemberSignRepository;
    @Autowired
    private CpMsgFactoryService cpMsgFactoryService;
    //内存实例
    public static final Map<String, Object> factoryPersistenceMap = new HashMap<String, Object>();

    /**
     * 2017年2月24日 21:44:18
     * 直接从内存中获取数据：
     * 1.如果内存中存在该条短信数据，那么直接返回
     * 2.如果内存中不存在该条短信数据，那么需要查询数据库后再进行返回
     * @param mapKey
     * @return
     */
    public synchronized Map<String, Object> getFactoryInfo(String mapKey, int factory)
    {
        Map<String, Object> factoryInfoMap = new HashMap<String, Object>();
        try
        {
            //查询一次数据库是否
            if(null == factoryPersistenceMap.get(mapKey + "=" + factory))
            {
                //内存中不存在厂家信息时，访问数据库
                List<CpMsgMemberSign> memberList = 
                        cpMsgMemberSignRepository.findByBrandIdAndIsUsed(
                                Integer.parseInt(mapKey), "Yes");
                CpMsgMemberSign cpMessageMemberSign = memberList.get(0);
                factoryInfoMap.put("_factory_username_", cpMessageMemberSign.getMsgUsername());//用户名
                factoryInfoMap.put("_factory_password_", cpMessageMemberSign.getMsgPassword());//密码
                factoryInfoMap.put("_factory_extno_", cpMessageMemberSign.getMsgExtno());//extno
                factoryInfoMap.put("_factory_sign_", cpMessageMemberSign.getSign());//签名
                factoryInfoMap.put("_factory_id_", cpMessageMemberSign.getFactoryId());//短信厂家信息
                //根据短信厂家编码主键获取短信厂家名称
                String factoryName = 
                        cpMsgFactoryService.getFactoryNameById(cpMessageMemberSign.getFactoryId());
                factoryInfoMap.put("_factory_name_", factoryName);
                factoryPersistenceMap.put(cpMessageMemberSign.getBrandId() + "=" + factory,
                        cpMessageMemberSign.getMsgUsername() + "%"
                                + cpMessageMemberSign.getMsgPassword() + "%"
                                + cpMessageMemberSign.getMsgExtno() + "%"
                                + cpMessageMemberSign.getSign() + "%"
                                + cpMessageMemberSign.getFactoryId() + "%"
                                + factoryName);
            }else
            {
                //内存中存在厂家信息时，直接拿出来
                String factoryInfoStr = factoryPersistenceMap.get(mapKey + "=" + factory).toString();
                String[] factoryInfoArr = factoryInfoStr.split("%");//注意拼装的格式
                factoryInfoMap.put("_factory_username_", factoryInfoArr[0]);//用户名
                factoryInfoMap.put("_factory_password_", factoryInfoArr[1]);//密码
                factoryInfoMap.put("_factory_extno_", factoryInfoArr[2]);//extno
                factoryInfoMap.put("_factory_sign_", factoryInfoArr[3]);//签名
                factoryInfoMap.put("_factory_id_", factoryInfoArr[4]);
                factoryInfoMap.put("_factory_name_", factoryInfoArr[5]);
            }
        }catch(Exception ex)
        {
            System.out.println("获取短信厂家出现异常 -> " + ex.getMessage());
        }
        return factoryInfoMap;
    }
}