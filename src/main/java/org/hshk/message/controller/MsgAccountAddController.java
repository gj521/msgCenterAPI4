package org.hshk.message.controller;

import com.google.gson.Gson;
import io.swagger.annotations.*;
import org.apache.log4j.Logger;
import org.hshk.message.BaseController;
import org.hshk.message.entity.CpMsgMember;
import org.hshk.message.entity.CpMsgMemberSign;
import org.hshk.message.outDto.OutMsgMemberSignList;
import org.hshk.message.repository.CpMsgMemberRepository;
import org.hshk.message.repository.CpMsgMemberSignRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Gaochuang on 2017/3/27.
 */


//新增 短信账号,添加短信商家表，的控制层


@Api(value = "短信账号 Api")
@RestController
@RequestMapping("/accounts")
public class MsgAccountAddController extends BaseController {
    //创建一个日志对象
    private static final Logger log = Logger.getLogger(MsgAccountAddController.class);
    @Autowired
    //查询短信厂家。和使用厂家的id，去查询单条记录
            CpMsgMemberSignRepository cpMsgMemberSignRepository;
    @Autowired
    //根据短信商的uid。去查询信息
            CpMsgMemberRepository cpMsgMemberRepository;
    BeanUtils beanUtils;
    //线程对象保存
    private static final ThreadLocal<Gson> local = new ThreadLocal<Gson>();

    //获取当前线程操作对象的使用权
    private Gson getGson() {
        if (null == local.get()) {
            Gson gson = new Gson();
            local.set(gson);
        }
        return local.get();
    }

    @ApiOperation(value = "添加短信账号,添加短信商家表")
    @ApiImplicitParams({})
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功"),
            @ApiResponse(code = 400, message = "请求参数不正确"),
            @ApiResponse(code = 404, message = "请求路径没有或路径不正确")
    })
    @ResponseBody
    //参数：访问路径，post请求，页面把你的数据当成json格式去解析
    @RequestMapping(value = "/addMsgAccount", method = RequestMethod.POST, produces = "application/json")

    //会员签名的添加方法。这个传进来的参数，是outDto里面的类
    public String MsgMemberAdd(@RequestBody OutMsgMemberSignList outMsgMemberSignList) {
        Map<String, Object> paramMsg = new HashMap<String, Object>();
        //获取当前时间
        int nowDate = (int) (System.currentTimeMillis() / 1000);
        try {
            log.info("MsgMemberSignAddController.MsgMemberSignAdd " + outMsgMemberSignList.toString());

            //获取商户签名
            String sign = outMsgMemberSignList.getSign();
            if (sign.indexOf("【") != 0 && sign.indexOf("】") != 0) {
                outMsgMemberSignList.setSign("【" + outMsgMemberSignList.getSign() + "】");
            }

            //创建用户商家表的对象
            CpMsgMemberSign cpMsgMemberSign = new CpMsgMemberSign();
            beanUtils.copyProperties(outMsgMemberSignList, cpMsgMemberSign);
            cpMsgMemberSign.setIsUsed("Yes");
            cpMsgMemberSign.setUpdateDate(nowDate);
            cpMsgMemberSign.setCreateDate(nowDate);

            cpMsgMemberSign = cpMsgMemberSignRepository.save(cpMsgMemberSign);
            paramMsg.put("cpMsgMemberSign", cpMsgMemberSign);


            //创建商家充值类
            CpMsgMember cpMsgMember = new CpMsgMember();
            beanUtils.copyProperties(outMsgMemberSignList, cpMsgMember);
            cpMsgMember.setTotalCount(0);
            cpMsgMember.setBalanceCount(0);
            cpMsgMember.setCreateDate(nowDate);
            cpMsgMember.setUpdateDate(nowDate);
            cpMsgMember = cpMsgMemberRepository.save(cpMsgMember);
            paramMsg.put("cpMsgMember", cpMsgMember);
            return getGson().toJson(paramMsg);
        } catch (Exception ex) {
            log.error("MsgMemberSignAddController.MsgMemberSignAdd" + ex.getMessage());
            ex.printStackTrace();
            Map<String, Object> resultMap = new HashMap<>();
            resultMap.put("resultCode", "fail");
            resultMap.put("resultMsg", "失败");
            return getGson().toJson(resultMap);
        }
    }
}
