package org.hshk.message.controller;

import com.google.gson.Gson;
import io.swagger.annotations.*;
import org.apache.log4j.Logger;
import org.hshk.message.BaseController;
import org.hshk.message.entity.CpMsgMember;
import org.hshk.message.entity.CpMsgMemberSign;
import org.hshk.message.entity.CpMsgPayDetail;
import org.hshk.message.entity.CpMsgPayruleDetail;
import org.hshk.message.repository.CpMsgMemberRepository;
import org.hshk.message.repository.CpMsgMemberSignRepository;
import org.hshk.message.repository.CpMsgPayDetailRepository;
import org.hshk.message.repository.MsgPayruleDetailRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2017/3/27.
 */
@Api(value = "充值历史 API")
@RestController
@RequestMapping("/payDetails")
public class MsgPayDetailAddController extends BaseController {
    private static final Logger log = Logger.getLogger(MsgPayDetailAddController.class);
    @Autowired
    //商家支付详情的保存 单表
            CpMsgPayDetailRepository cpMsgPayDetailRepository;
    @Autowired
    //短信厂商查询 单表
            CpMsgMemberSignRepository cpMsgMemberSignRepository;
    @Autowired
    //根据id，查询充值规则信息
            MsgPayruleDetailRepository msgPayruleDetailRepository;
    @Autowired
    //商户信息查询
            CpMsgMemberRepository cpMsgMemberRepository;
    private BeanUtils beanUtils;
    //线程对象保存
    private static final ThreadLocal<Gson> local = new ThreadLocal<Gson>();

    //获取当前线程操作对象的使用权
    private Gson getGson() {
        if (null == local.get()) {
            Gson gson = new Gson();
            local.set(gson);
        }
        return local.get();
    }

    @ApiOperation(value = "新增充值记录")
    @ApiImplicitParams({
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "充值记录成功"),
            @ApiResponse(code = 400, message = "请求参数不正确"),
            @ApiResponse(code = 404, message = "请求路径没有或路径不正确")
    })
    @ResponseBody
    @RequestMapping(value = "add", method = RequestMethod.POST, produces = "application/json")

    public String MsgRecord(@RequestBody CpMsgPayDetail cpMsgPayDetail) {
        int nowDate = (int) (System.currentTimeMillis() / 1000);
        Map<String, Object> paramMsg = new HashMap<String, Object>();
        try {
            log.info("MsgPayDetailAddController.MsgRecord" + "cpMsgPayDetail" + cpMsgPayDetail.toString());
            CpMsgMemberSign cpMsgMemberSign = cpMsgMemberSignRepository.findByBrandIdAndOrgIdAndIsUsed(cpMsgPayDetail.getBrandId(), cpMsgPayDetail.getUid(), "Yes");
            if (null == cpMsgMemberSign) {
                log.error("MsgMemberAddController.msgMembersDirectRecharge " + "cpMsgMemberSign 表参数不存在");
                paramMsg.put("resultCode", "fail");
                paramMsg.put("resultMsg", "cpMsgMemberSign表参数不存在");
                return getGson().toJson(paramMsg);
            }
            CpMsgPayruleDetail cpMsgPayruleDetail = msgPayruleDetailRepository.findById(cpMsgPayDetail.getPayruleId());
            if (null == cpMsgPayruleDetail) {
                log.info("MsgMemberAddController.msgMembersDirectRecharge " + "msgPayruleDetail表参数不存在");
                paramMsg.put("resultMsg", "msgPayruleDetail表参数不存在");
                return getGson().toJson(paramMsg);
            }
            CpMsgMember cpMsgMember = cpMsgMemberRepository.findByOrgIdAndBrandId(cpMsgPayDetail.getUid(), cpMsgPayDetail.getBrandId());
            if (null == cpMsgMember) {
                log.error("MsgMemberAddController.msgMembersDirectRecharge " + "cpMsgMember表参数不存在");
                paramMsg.put("resultMsg", "cpMsgMember表参数不存在");
                return getGson().toJson(paramMsg);
            }
            if (2 == cpMsgMemberSign.getFactoryId()) {
                CpMsgMember cpMsgMember1 = new CpMsgMember();
                beanUtils.copyProperties(cpMsgMember, cpMsgMember1);
                cpMsgMember1.setUpdateDate(nowDate);
                cpMsgMember1.setBalanceCount(cpMsgMember.getBalanceCount());
                cpMsgMember1.setTotalCount(cpMsgMember.getTotalCount() + cpMsgPayruleDetail.getCount());
                cpMsgMember1 = cpMsgMemberRepository.save(cpMsgMember1);
                cpMsgPayDetail.setPayCount(cpMsgPayruleDetail.getCount());
                cpMsgPayDetail.setPayAmount(cpMsgPayruleDetail.getAmount());
                cpMsgPayDetail.setStatus("finished");
                cpMsgPayDetail.setFactoryId(2);
                cpMsgPayDetail.setUpdateDate(nowDate);
                cpMsgPayDetail.setCreateDate(nowDate);
                cpMsgPayDetailRepository.save(cpMsgPayDetail);
                return getGson().toJson(cpMsgMember1);
            } else {
                cpMsgPayDetail.setPayCount(cpMsgPayruleDetail.getCount());
                cpMsgPayDetail.setPayAmount(cpMsgPayruleDetail.getAmount());
                cpMsgPayDetail.setStatus("payed");
                cpMsgPayDetail.setFactoryId(1);
                cpMsgPayDetail.setUpdateDate(nowDate);
                cpMsgPayDetail.setCreateDate(nowDate);
                cpMsgPayDetail = cpMsgPayDetailRepository.save(cpMsgPayDetail);
                return getGson().toJson(cpMsgPayDetail);
            }

        } catch (Exception ex) {
            log.error("MsgPayDetailAddController.MsgRecord" + ex.getMessage());
            ex.printStackTrace();
            Map<String, Object> resultMap = new HashMap<>();
            resultMap.put("resultCode", "fail");
            resultMap.put("resultMsg", "失败");
            return getGson().toJson(resultMap);
        }
    }
}
