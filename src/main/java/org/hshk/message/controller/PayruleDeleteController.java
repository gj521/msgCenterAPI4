package org.hshk.message.controller;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import io.swagger.annotations.*;
import org.apache.log4j.Logger;
import org.hshk.message.BaseController;
import org.hshk.message.entity.CpMsgMember;
import org.hshk.message.entity.CpMsgPayrule;
import org.hshk.message.repository.CpMsgMemberRepository;
import org.hshk.message.repository.CpMsgPayruleDetailRepository;
import org.hshk.message.repository.CpMsgPayruleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by yxl on 2017-03-28.
 */
@Api(value = "充值规则 API")
@RestController
@RequestMapping("/payrules")
public class PayruleDeleteController extends BaseController {
    private static final Logger log = Logger.getLogger(PayruleDeleteController.class);
    @Autowired
    CpMsgPayruleRepository cpMsgPayruleRepository;
    @Autowired
    CpMsgPayruleDetailRepository cpMsgPayruleDetailRepository;
    @Autowired
    CpMsgMemberRepository cpMsgMemberRepository;
    private static final ThreadLocal<Gson> local = new ThreadLocal<Gson>();

    private Gson getGson() {
        if (null == local.get()) {
            Gson gson = new Gson();
            local.set(gson);
        }
        return local.get();
    }

    @ApiOperation(value = "删除充值规则")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "payruleId", value = "充值规则id", required = true, dataType = "int", paramType = "path")
    })
    @ApiResponses({
            @ApiResponse(code = 400, message = "请求参数不正确"),
            @ApiResponse(code = 404, message = "请求路径没有或路径不正确")
    })
    @ResponseBody
    @RequestMapping(value = "/{payruleId}", method = RequestMethod.DELETE, produces = "application/json")
    public String deletePayrule(@PathVariable int payruleId) {
        Map<String, Object> resultMap = new HashMap<>();
        try {
            log.info("PayruleDeleteController.deletePayrule [ payruleId = " + payruleId + " ]");
            Map<String, Object> paramMap = new HashMap<String, Object>();
            JSONObject resultJson = new JSONObject();
            if (0 == payruleId) {
                paramMap.put("payruleId", "为必填参数");
            }
            if (0 != paramMap.size()) {
                return getGson().toJson(paramMap);
            }
            List<CpMsgMember> cpMsgMembers = cpMsgMemberRepository.findByPayruleId(payruleId);
            if(null != cpMsgMembers && cpMsgMembers.size() > 0){
                resultMap.put("resultCode", "delete_fail");
                resultMap.put("resultMsg", "删除失败，该充值规则已被引用");
                return getGson().toJson(resultMap);
            }
            cpMsgPayruleRepository.delete(payruleId);
            cpMsgPayruleDetailRepository.deleteByPayruleId(payruleId);
            resultMap.put("resultCode", "success");
            resultMap.put("resultMsg", "成功");
            return getGson().toJson(resultMap);
        } catch (Exception ex) {
            //出现异常时
            log.error("PayruleDeleteController.deletePayrule " + ex.getMessage());
            ex.printStackTrace();
            resultMap.put("resultCode", "fail");
            resultMap.put("resultMsg", "失败");
            return getGson().toJson(resultMap);
        }
    }
}
