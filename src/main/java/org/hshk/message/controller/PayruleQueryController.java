package org.hshk.message.controller;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import io.swagger.annotations.*;
import org.apache.log4j.Logger;
import org.hshk.message.BaseController;
import org.hshk.message.entity.CpMsgMember;
import org.hshk.message.entity.CpMsgPayrule;
import org.hshk.message.entity.CpMsgPayruleDetail;
import org.hshk.message.inDto.InDtoPayrule;
import org.hshk.message.repository.CpMsgMemberRepository;
import org.hshk.message.repository.CpMsgPayruleDetailRepository;
import org.hshk.message.repository.CpMsgPayruleRepository;
import org.hshk.message.service.PayruleService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by yxl on 2017-03-28.
 */
@Api(value = "充值规则 API")
@RestController
@RequestMapping("/payrules")
public class PayruleQueryController extends BaseController {
    private static final Logger log = Logger.getLogger(PayruleUpdateController.class);
    @Autowired
    PayruleService payruleService;
    @Autowired
    CpMsgPayruleRepository cpMsgPayruleRepository;
    @Autowired
    CpMsgPayruleDetailRepository cpMsgPayruleDetailRepository;
    @Autowired
    CpMsgMemberRepository cpMsgMemberRepository;

    private static final ThreadLocal<Gson> local = new ThreadLocal<Gson>();
    private BeanUtils beanUtils;

    private Gson getGson() {
        if (null == local.get()) {
            Gson gson = new Gson();
            local.set(gson);
        }
        return local.get();
    }

    @ApiOperation(value = "获取充值规则列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页码", required = true, dataType = "int", paramType = "path"),
            @ApiImplicitParam(name = "pageRecorders", value = "每页行数", required = true, dataType = "int", paramType = "path")
    })
    @ApiResponses({
            @ApiResponse(code = 400, message = "请求参数不正确"),
            @ApiResponse(code = 404, message = "请求路径没有或路径不正确")
    })
    @ResponseBody
    @RequestMapping(value = "/page/{page}/pageRecorders/{pageRecorders}", method = RequestMethod.GET, produces = "application/json")
    public String getPayrules(@PathVariable int page, @PathVariable int pageRecorders) {
        try {
            log.info("PayruleQueryController.getPayrules [ ]");
            //参数集合
            Map<String, Object> paramMap = new HashMap<String, Object>();
            JSONObject resultJson = new JSONObject();
            //参数校验
            //当前页码
            if (0 == page) {
                paramMap.put("page", "为必填参数");
            }
            //每页行数
            if (0 == pageRecorders) {
                paramMap.put("pageRecorders", "为必填参数");
            }
            //参数出错
            if (0 != paramMap.size()) {
                return getGson().toJson(paramMap);
            }

            resultJson.put("totalSize", payruleService.getPayrules(paramMap).size());
            paramMap.put("startPoint", (page - 1) * pageRecorders);
            paramMap.put("endPoint", pageRecorders);
            resultJson.put("list", payruleService.getPayrules(paramMap));

            return getGson().toJson(resultJson);
        } catch (Exception ex) {
            //出现异常时
            log.error("PayruleUpdateController.updatePayrule " + ex.getMessage());
            ex.printStackTrace();
            Map<String, Object> resultMap = new HashMap<>();
            resultMap.put("resultCode", "fail");
            resultMap.put("resultMsg", "失败");
            return getGson().toJson(resultMap);
        }
    }

    @ApiOperation(value = "根据id获取充值规则详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "payruleId", value = "充值规则id", required = true, dataType = "int", paramType = "path")
    })
    @ApiResponses({
            @ApiResponse(code = 400, message = "请求参数不正确"),
            @ApiResponse(code = 404, message = "请求路径没有或路径不正确")
    })
    @ResponseBody
    @RequestMapping(value = "/payruleId/{payruleId}", method = RequestMethod.GET, produces = "application/json")
    public String getPayruleById(@PathVariable int payruleId) {
        try {
            log.info("PayruleQueryController.getPayruleById [ payruleId = " + payruleId + " ]");
            Map<String, Object> paramMap = new HashMap<String, Object>();
            JSONObject resultJson = new JSONObject();
            if (0 == payruleId) {
                paramMap.put("payruleId", "为必填参数");
            }
            if (0 != paramMap.size()) {
                return getGson().toJson(paramMap);
            }
            List<InDtoPayrule> inDtoPayruleList = new ArrayList<InDtoPayrule>();
            InDtoPayrule inDtoPayrule = new InDtoPayrule();
            CpMsgPayrule payrule = cpMsgPayruleRepository.getOne(payruleId);
            beanUtils.copyProperties(payrule, inDtoPayrule);
            inDtoPayrule.setDetailList(cpMsgPayruleDetailRepository.getByPayruleId(payruleId));
            inDtoPayruleList.add(inDtoPayrule);
            return getGson().toJson(inDtoPayruleList);
        } catch (Exception ex) {
            //出现异常时
            log.error("PayruleQueryController.updatePayrule " + ex.getMessage());
            ex.printStackTrace();
            Map<String, Object> resultMap = new HashMap<>();
            resultMap.put("resultCode", "fail");
            resultMap.put("resultMsg", "失败");
            return getGson().toJson(resultMap);
        }
    }

    @ApiOperation(value = "根据商户和品牌获取充值规则明细")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orgId", value = "商户id", required = true, dataType = "int", paramType = "path"),
            @ApiImplicitParam(name = "brandId", value = "品牌id", required = true, dataType = "int", paramType = "path")
    })
    @ApiResponses({
            @ApiResponse(code = 400, message = "请求参数不正确"),
            @ApiResponse(code = 404, message = "请求路径没有或路径不正确")
    })
    @ResponseBody
    @RequestMapping(value = "/orgId/{orgId}/brandId/{brandId}", method = RequestMethod.GET, produces = "application/json")
    public String getPayruleDetailByOrgIdAndBrandId(@PathVariable int orgId, @PathVariable int brandId) {
        try {
            log.info("PayruleQueryController.getPayruleDetailByOrgIdAndBrandId [ orgId = " + orgId + "brandId = " + brandId + " ]");
            Map<String, Object> resultMap = new HashMap<String, Object>();
            JSONObject resultJson = new JSONObject();
            if (0 == orgId) {
                resultMap.put("orgId", "为必填参数");
            }
            if (0 == brandId) {
                resultMap.put("brandId", "为必填参数");
            }
            if (0 != resultMap.size()) {
                return getGson().toJson(resultMap);
            }
            CpMsgMember cpMsgMember = cpMsgMemberRepository.findByOrgIdAndBrandId(orgId, brandId);
            List<CpMsgPayruleDetail> payruleDetailList = new ArrayList<CpMsgPayruleDetail>();
            if (null != cpMsgMember) {
                payruleDetailList = cpMsgPayruleDetailRepository.getByPayruleId(cpMsgMember.getPayruleId());
            }
            return getGson().toJson(payruleDetailList);
        } catch (Exception ex) {
            //出现异常时
            log.error("PayruleQueryController.getPayruleDetailByOrgIdAndBrandId " + ex.getMessage());
            ex.printStackTrace();
            Map<String, Object> resultMap = new HashMap<>();
            resultMap.put("resultCode", "fail");
            resultMap.put("resultMsg", "失败");
            return getGson().toJson(resultMap);
        }
    }

}
