package org.hshk.message.controller;

import com.google.gson.Gson;
import io.swagger.annotations.*;
import org.apache.log4j.Logger;
import org.hshk.message.BaseController;
import org.hshk.message.entity.CpMsgPayrule;
import org.hshk.message.entity.CpMsgPayruleDetail;
import org.hshk.message.inDto.InDtoPayrule;
import org.hshk.message.repository.CpMsgPayruleDetailRepository;
import org.hshk.message.repository.CpMsgPayruleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by yxl on 2017-03-28.
 */
@Api(value = "充值规则 API")
@RestController
@RequestMapping("/payrules")
public class PayruleUpdateController extends BaseController {
    private static final Logger log = Logger.getLogger(PayruleUpdateController.class);
    @Autowired
    private CpMsgPayruleRepository cpMsgPayruleRepository;
    @Autowired
    private CpMsgPayruleDetailRepository cpMsgPayruleDetailRepository;
    private static final ThreadLocal<Gson> local = new ThreadLocal<>();

    private Gson getGson() {
        if (null == local.get()) {
            Gson gson = new Gson();
            local.set(gson);
        }
        return local.get();
    }

    @ApiOperation(value = "修改充值规则")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "payruleId", value = "充值规则id", required = true, dataType = "int", paramType = "path"),
            @ApiImplicitParam(name = "inDtoPayrule", value = "充值规则dto", required = true, dataType = "InDtoPayrule")
    })
    @ApiResponses({
            @ApiResponse(code = 400, message = "请求参数不正确"),
            @ApiResponse(code = 404, message = "请求路径没有或路径不正确")
    })
    @ResponseBody
    @RequestMapping(value = "/{payruleId}", method = RequestMethod.PUT, produces = "application/json")
    public String updatePayrule(@PathVariable int payruleId, @RequestBody InDtoPayrule inDtoPayrule) {
        try {
            log.info("PayruleUpdateController.updatePayrule [ " + inDtoPayrule.toString() + " ]");
            Map<String, Object> paramMap = new HashMap<>();
            if (0 == payruleId) {
                paramMap.put("payruleId", "为必填参数");
            }
            if (0 != paramMap.size()) {
                return getGson().toJson(paramMap);
            }
            int nowDate = (int) (System.currentTimeMillis() / 1000);
            CpMsgPayrule payrule = new CpMsgPayrule();
            payrule.setId(payruleId);
            payrule.setPayruleName(inDtoPayrule.getPayruleName());
            payrule.setBeginExpiryDate(inDtoPayrule.getBeginExpiryDate());
            payrule.setEndExpiryDate(inDtoPayrule.getEndExpiryDate());
            payrule.setCreateDate(nowDate);
            payrule.setUpdateDate(nowDate);
            payrule = cpMsgPayruleRepository.save(payrule);

            cpMsgPayruleDetailRepository.deleteByPayruleId(payruleId);

            for (CpMsgPayruleDetail dtoDetail : inDtoPayrule.getDetailList()) {
                CpMsgPayruleDetail detail = new CpMsgPayruleDetail();
                detail.setPayruleId(payruleId);
                detail.setCount(dtoDetail.getCount());
                detail.setAmount(dtoDetail.getAmount());
                detail.setCreateDate(nowDate);
                detail.setUpdateDate(nowDate);
                cpMsgPayruleDetailRepository.save(detail);
            }
            return getGson().toJson(payrule);

        } catch (Exception ex) {
            //出现异常时
            log.error("PayruleUpdateController.updatePayrule " + ex.getMessage());
            ex.printStackTrace();
            Map<String, Object> resultMap = new HashMap<>();
            resultMap.put("resultCode", "fail");
            resultMap.put("resultMsg", "失败");
            return getGson().toJson(resultMap);
        }
    }
}
