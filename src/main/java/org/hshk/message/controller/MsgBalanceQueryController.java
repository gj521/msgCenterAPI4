package org.hshk.message.controller;

import com.google.gson.Gson;
import io.swagger.annotations.*;
import org.apache.log4j.Logger;
import org.hshk.message.BaseController;
import org.hshk.message.entity.CpMsgMember;
import org.hshk.message.repository.CpMsgMemberRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by GaoChuang on 2017/3/25.
 */
@Api(value = "短信余额 API")
@RestController
@RequestMapping("/balances")

//这是剩余短信条数查询的方法
public class MsgBalanceQueryController extends BaseController{
    private static final Logger log = Logger.getLogger(MsgBalanceQueryController.class);

    private static final ThreadLocal<Gson> local = new ThreadLocal<Gson>();

    @Autowired
    CpMsgMemberRepository cpMsgMemberRepository;
    private Gson getGson()
    {
        if(null == local.get())
        {
            Gson gson = new Gson();
            local.set(gson);
        }
        return local.get();
    }
    @ApiOperation(value = "短信剩余条数查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orgId", value = "商户id", required = true, dataType = "int", paramType = "path"),
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "短信余额充值成功"),
            @ApiResponse(code = 400, message = "请求参数不正确"),
            @ApiResponse(code = 404, message = "请求路径没有或路径不正确")

    })
    @ResponseBody
    @RequestMapping(value = "orgId/{orgId}", method = RequestMethod.GET, produces = "application/json" )

    public String overagePost(@PathVariable int orgId){
        try{
            log.info("MsgOverageController.overagePost" + "orgId" + orgId);
            Map<String, Object> paramMsg = new HashMap<String, Object>();
            if(0==orgId){
                paramMsg.put("orgId","参数不正确");
            }
            if(0!=paramMsg.size()){
                return getGson().toJson(paramMsg);
            }
            List<CpMsgMember> cpMsgMember = cpMsgMemberRepository.findByOrgId(orgId);
            return getGson().toJson(cpMsgMember);
        }catch (Exception ex){
            //出现异常时
            log.error("MsgOverageController.overagePost" + ex.getMessage());
            ex.printStackTrace();
            Map<String, Object> resultMap = new HashMap<>();
            resultMap.put("resultCode", "fail");
            resultMap.put("resultMsg", "失败");
            return getGson().toJson(resultMap);
        }

    }

}
