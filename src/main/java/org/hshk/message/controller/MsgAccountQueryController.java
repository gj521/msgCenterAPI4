package org.hshk.message.controller;

import com.google.gson.Gson;
import io.swagger.annotations.*;
import org.apache.log4j.Logger;
import org.hshk.message.BaseController;
import org.hshk.message.entity.CpMsgMember;
import org.hshk.message.entity.CpMsgMemberSign;
import org.hshk.message.outDto.OutMsgMemberSignList;
import org.hshk.message.repository.CpMsgMemberRepository;
import org.hshk.message.repository.CpMsgMemberSignRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/3/27.
 */
@Api(value = "短信账号 Api")
@RestController
@RequestMapping("/accounts")



public class MsgAccountQueryController extends BaseController {
    private static final Logger log = Logger.getLogger(MsgAccountQueryController.class);

    @Autowired
    CpMsgMemberSignRepository cpMsgMemberSignRepository;

    @Autowired
    CpMsgMemberRepository cpMsgMemberRepository;

    BeanUtils beanUtils;

    private static final ThreadLocal<Gson> local = new ThreadLocal<Gson>();

    private Gson getGson() {
        if (null == local.get()) {
            Gson gson = new Gson();
            local.set(gson);
        }
        return local.get();
    }

    @ApiOperation(value = "根据orgId和brandId查询所有短信账号")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orgId", value = "商户id", required = true, dataType = "int", paramType = "path"),
            @ApiImplicitParam(name = "brandId", value = "品牌id", required = true, dataType = "int", paramType = "path"),

    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功"),
            @ApiResponse(code = 400, message = "请求参数不正确"),
            @ApiResponse(code = 404, message = "请求路径没有或路径不正确")
    })
    @ResponseBody
    @RequestMapping(value = "/orgId/{orgId}/brandId/{brandId}", method = RequestMethod.GET, produces = "application/json")

    public String memberSignAccountInformation(@PathVariable("orgId") int orgId, @PathVariable("brandId") int brandId) {
        Map<String, Object> paramMsg = new HashMap<String, Object>();
        try {
            log.info("MsgMemberSignQueryController.memberSignAccountInformation" + "orgId" + orgId + "brandId" + brandId);
            if (0 == orgId) {
                paramMsg.put("orgId", "参数不对");
            }
            if (0 == brandId) {
                paramMsg.put("brandId", "参数不对");
            }
            if (0 != paramMsg.size()) {
                return getGson().toJson(paramMsg);
            }
            List<CpMsgMemberSign> cpMsgMemberSign = cpMsgMemberSignRepository.findByBrandIdAndOrgId(brandId, orgId);
            if (null == cpMsgMemberSign) {
                log.error("MsgMemberSignQueryController.memberSignAccountInformation" + "cpMsgMemberSign表参数不存在");
                paramMsg.put("resultCode", "fail");
                paramMsg.put("resultMsg", "cpMsgMemberSign表参数不存在");
                return getGson().toJson(paramMsg);
            }
            CpMsgMember cpMsgMember = cpMsgMemberRepository.findByOrgIdAndBrandId(orgId, brandId);
            if (null == cpMsgMember) {
                log.error("MsgMemberSignQueryController.memberSignAccountInformation" + "cpMsgMember表参数不存在");
                paramMsg.put("resultCode", "fail");
                paramMsg.put("resultMsg", "cpMsgMember表参数不存在");
                return getGson().toJson(paramMsg);
            }
            //创建一个返回数据创建的实体类 的集合
            List<OutMsgMemberSignList> list = new ArrayList<OutMsgMemberSignList>();
            for (CpMsgMemberSign cpMsgMemberSign1 : cpMsgMemberSign) {
                OutMsgMemberSignList outMsgMemberSignList = new OutMsgMemberSignList();

                beanUtils.copyProperties(cpMsgMemberSign1, outMsgMemberSignList);
                outMsgMemberSignList.setPayruleId(cpMsgMember.getPayruleId());
                outMsgMemberSignList.setAlarmMobiles(cpMsgMember.getAlarmMobiles());
                outMsgMemberSignList.setSign(cpMsgMemberSign1.getSign().replaceAll("【", "").replaceAll("】", ""));
                list.add(outMsgMemberSignList);
            }
            return getGson().toJson(list);
        } catch (Exception ex) {
            log.error("MsgMemberSignQueryController.memberSignAccountInformation" + ex.getMessage());
            ex.printStackTrace();
            Map<String, Object> resultMap = new HashMap<>();
            resultMap.put("resultCode", "fail");
            resultMap.put("resultMsg", "失败");
            return getGson().toJson(resultMap);
        }
    }


}
