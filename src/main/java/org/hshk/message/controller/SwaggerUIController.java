package org.hshk.message.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created With IntelliJ IDEA
 * User: WangYong
 * Date: 2017/2/7 0007
 * Time: 08:47
 * ProjectName: HSHK-JAVA-SERV-ORG
 * To change this template use File
 */
@Controller
public class SwaggerUIController
{
    
    @RequestMapping(value = "swagger")
    public String index()
    {
        System.out.println("swagger-ui.html");
        return "redirect:swagger-ui.html";
    }
    
    
    
}
