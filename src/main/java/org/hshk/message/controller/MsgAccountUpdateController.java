package org.hshk.message.controller;

import com.google.gson.Gson;
import io.swagger.annotations.*;
import org.apache.log4j.Logger;
import org.hshk.message.BaseController;
import org.hshk.message.entity.CpMsgMember;
import org.hshk.message.entity.CpMsgMemberSign;
import org.hshk.message.outDto.OutMsgMemberSignList;
import org.hshk.message.repository.CpMsgMemberRepository;
import org.hshk.message.repository.CpMsgMemberSignRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by GaoChuang on 2017/3/27.
 */
@Api(value = "短信账号 Api")
@RestController
@RequestMapping("/accounts")


//修改短信商家的信息（不是短信厂商）

public class MsgAccountUpdateController extends BaseController {
    private static final Logger log = Logger.getLogger(MsgAccountUpdateController.class);
    @Autowired
    //查询短信厂家。和使用厂家的id去查询单条记录，的单表查询对象
            CpMsgMemberSignRepository cpMsgMemberSignRepository;

    //短信商家的mapper接口
    //@Autowired
    //CpMsgMemberMapper cpMsgMemberMapper;

    @Autowired
    CpMsgMemberRepository cpMsgMemberRepository;
    BeanUtils beanUtils;
    private static final ThreadLocal<Gson> local = new ThreadLocal<Gson>();

    private Gson getGson() {
        if (null == local.get()) {
            Gson gson = new Gson();
            local.set(gson);
        }
        return local.get();
    }

    @ApiOperation(value = "更换厂商，前提是都有短信账号和密码的情况下")
    @ApiImplicitParams({
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "成功"),
            @ApiResponse(code = 400, message = "请求参数不正确"),
            @ApiResponse(code = 404, message = "请求路径没有或路径不正确")
    })
    @ResponseBody
    @RequestMapping(value = "upDate", method = RequestMethod.POST, produces = "application/json")

    //更换签名的方法（传进来的对象是 短信商家类（outDto中的类））
    public String signUpdate(@RequestBody OutMsgMemberSignList outMsgMemberSignList) {
        Map<String, Object> paramMsg = new HashMap<String, Object>();
        //获取当前时间
        int nowDate = (int) (System.currentTimeMillis() / 1000);
        try {
            log.info("MsgAccountUpdateController.signUpdate" +
                    "outMsgMemberSignList" + outMsgMemberSignList.toString());
            String sign = outMsgMemberSignList.getSign();
            if (sign.indexOf("【") != 0 && sign.indexOf("】") != 0) {
                outMsgMemberSignList.setSign("【" + outMsgMemberSignList.getSign() + "】");
            }
            System.err.println(outMsgMemberSignList.getAlarmMobiles() + ":手机号为");
            if (0 != outMsgMemberSignList.getId()) {
                if (1 == outMsgMemberSignList.getFactoryId()) {


                    CpMsgMemberSign cpMsgMemberSign = new CpMsgMemberSign();

                    beanUtils.copyProperties(outMsgMemberSignList, cpMsgMemberSign);
                    cpMsgMemberSign.setUpdateDate(nowDate);
                    cpMsgMemberSign.setIsUsed("Yes");
                    cpMsgMemberSign = cpMsgMemberSignRepository.save(cpMsgMemberSign);
                    System.err.println(cpMsgMemberSign.getSign());
                    CpMsgMemberSign cpMsgMemberSign1 = cpMsgMemberSignRepository.findByBrandIdAndFactoryId(outMsgMemberSignList.getBrandId(), 2);
                    if (null != cpMsgMemberSign1) {
                        cpMsgMemberSign1.setIsUsed("No");
                        cpMsgMemberSign1.setUpdateDate(nowDate);
                        cpMsgMemberSignRepository.save(cpMsgMemberSign1);
                    }

                    /**
                     * Creat by yyl
                     * 2017年6月22日14:15:04
                     * mapper转jpa
                     */
                    cpMsgMemberRepository.updateByorgIdAndbrandIdAndalarmMobiles(outMsgMemberSignList.getPayruleId(), outMsgMemberSignList.getOrgId(), outMsgMemberSignList.getBrandId(), outMsgMemberSignList.getAlarmMobiles());

                    List<CpMsgMember> cpMsgMember = cpMsgMemberRepository.findByBrandId(outMsgMemberSignList.getBrandId());
                    if (null == cpMsgMember || 0 >= cpMsgMember.size()) {
                        paramMsg.put("resultCode", "fail");
                        paramMsg.put("resultMsg", "cpMsgMember表无相关数据");
                        return getGson().toJson(paramMsg);
                    }
                    paramMsg.put("resultCode", "success");
                    cpMsgMemberSign.setSign(cpMsgMemberSign.getSign().replaceAll("【", "").replaceAll("】", ""));
                    paramMsg.put("cpMsgMemberSign", cpMsgMemberSign);
                    paramMsg.put("cpMsgMember", cpMsgMember.get(0));
                    return getGson().toJson(paramMsg);
                } else {
                    CpMsgMemberSign cpMsgMemberSign = new CpMsgMemberSign();
                    beanUtils.copyProperties(outMsgMemberSignList, cpMsgMemberSign);
                    cpMsgMemberSign.setUpdateDate(nowDate);
                    cpMsgMemberSign.setIsUsed("Yes");
                    cpMsgMemberSign = cpMsgMemberSignRepository.save(cpMsgMemberSign);
                    CpMsgMemberSign cpMsgMemberSign1 = cpMsgMemberSignRepository.findByBrandIdAndFactoryId(outMsgMemberSignList.getBrandId(), 1);
                    if (null != cpMsgMemberSign1) {
                        cpMsgMemberSign1.setIsUsed("No");
                        cpMsgMemberSign1.setUpdateDate(nowDate);
                        cpMsgMemberSignRepository.save(cpMsgMemberSign1);
                    }
                    //3、修改短信商户表中充值规则mapper
                    //cpMsgMemberMapper.upDateByPayruleId(outMsgMemberSignList.getOrgId(), outMsgMemberSignList.getBrandId(), outMsgMemberSignList.getPayruleId(), outMsgMemberSignList.getAlarmMobiles());
                    /**
                     * Creat by yyl
                     * 2017年6月22日14:15:04
                     * mapper转jpa
                     */
                    cpMsgMemberRepository.updateByorgIdAndbrandIdAndalarmMobiles(outMsgMemberSignList.getPayruleId(), outMsgMemberSignList.getOrgId(), outMsgMemberSignList.getBrandId(), outMsgMemberSignList.getAlarmMobiles());


                    List<CpMsgMember> cpMsgMember = cpMsgMemberRepository.findByBrandId(outMsgMemberSignList.getBrandId());
                    if (null == cpMsgMember || 0 >= cpMsgMember.size()) {
                        paramMsg.put("resultCode", "fail");
                        paramMsg.put("resultMsg", "cpMsgMember表无相关数据");
                        return getGson().toJson(paramMsg);
                    }
                    paramMsg.put("cpMsgMember", cpMsgMember.get(0));
                    paramMsg.put("resultCode", "success");
                    cpMsgMemberSign.setSign(cpMsgMemberSign.getSign().replaceAll("【", "").replaceAll("】", ""));
                    paramMsg.put("cpMsgMemberSign", cpMsgMemberSign);
                    return getGson().toJson(paramMsg);
                }
            } else {
                if (1 == outMsgMemberSignList.getFactoryId()) {
                    String requestSign = outMsgMemberSignList.getSign();
                    CpMsgMemberSign cpMsgMemberSign = new CpMsgMemberSign();
                    beanUtils.copyProperties(outMsgMemberSignList, cpMsgMemberSign);
                    cpMsgMemberSign.setUpdateDate(nowDate);
                    cpMsgMemberSign.setCreateDate(nowDate);
                    cpMsgMemberSign.setIsUsed("Yes");
                    cpMsgMemberSign = cpMsgMemberSignRepository.save(cpMsgMemberSign);
                    CpMsgMemberSign cpMsgMemberSign1 = cpMsgMemberSignRepository.findByBrandIdAndFactoryId(outMsgMemberSignList.getBrandId(), 2);
                    if (null != cpMsgMemberSign1) {
                        cpMsgMemberSign1.setIsUsed("No");
                        cpMsgMemberSign1.setUpdateDate(nowDate);
                        cpMsgMemberSignRepository.save(cpMsgMemberSign1);
                    }
                    //3、修改短信商户表中充值规则mapper
                    //cpMsgMemberMapper.upDateByPayruleId(outMsgMemberSignList.getOrgId(), outMsgMemberSignList.getBrandId(), outMsgMemberSignList.getPayruleId(), outMsgMemberSignList.getAlarmMobiles());
                    /**
                     * Creat by yyl
                     * 2017年6月22日14:15:04
                     * mapper转jpa
                     */
                    cpMsgMemberRepository.updateByorgIdAndbrandIdAndalarmMobiles(outMsgMemberSignList.getPayruleId(), outMsgMemberSignList.getOrgId(), outMsgMemberSignList.getBrandId(), outMsgMemberSignList.getAlarmMobiles());

                    List<CpMsgMember> cpMsgMember = cpMsgMemberRepository.findByBrandId(outMsgMemberSignList.getBrandId());
                    if (null == cpMsgMember || 0 >= cpMsgMember.size()) {
                        paramMsg.put("resultCode", "fail");
                        paramMsg.put("resultMsg", "cpMsgMember表无相关数据");
                        return getGson().toJson(paramMsg);
                    }
                    paramMsg.put("cpMsgMember", cpMsgMember.get(0));
                    paramMsg.put("resultCode", "success");
                    cpMsgMemberSign.setSign(cpMsgMemberSign.getSign().replaceAll("【", "").replaceAll("】", ""));
                    paramMsg.put("cpMsgMemberSign", cpMsgMemberSign);
                    return getGson().toJson(paramMsg);
                } else {
                    String requestSign = outMsgMemberSignList.getSign();
                    System.err.println(requestSign);
                    if (requestSign.indexOf("【") != 0 && requestSign.indexOf("】") != 0) {
                        outMsgMemberSignList.setSign("【" + outMsgMemberSignList.getSign() + "】");
                    }
                    System.err.println(requestSign);
                    CpMsgMemberSign cpMsgMemberSign = new CpMsgMemberSign();
                    beanUtils.copyProperties(outMsgMemberSignList, cpMsgMemberSign);
                    cpMsgMemberSign.setUpdateDate(nowDate);
                    cpMsgMemberSign.setIsUsed("Yes");
                    cpMsgMemberSign = cpMsgMemberSignRepository.save(cpMsgMemberSign);
                    CpMsgMemberSign cpMsgMemberSign1 = cpMsgMemberSignRepository.findByBrandIdAndFactoryId(outMsgMemberSignList.getBrandId(), 1);
                    if (null != cpMsgMemberSign1) {
                        cpMsgMemberSign1.setIsUsed("No");
                        cpMsgMemberSign1.setUpdateDate(nowDate);
                        cpMsgMemberSignRepository.save(cpMsgMemberSign1);
                    }
                    //3、修改短信商户表中充值规则mapper
                    //cpMsgMemberMapper.upDateByPayruleId(outMsgMemberSignList.getOrgId(), outMsgMemberSignList.getBrandId(), outMsgMemberSignList.getPayruleId(), outMsgMemberSignList.getAlarmMobiles());
                    /**
                     * Creat by yyl
                     * 2017年6月22日14:15:04
                     * mapper转jpa
                     */
                    cpMsgMemberRepository.updateByorgIdAndbrandIdAndalarmMobiles(outMsgMemberSignList.getPayruleId(), outMsgMemberSignList.getOrgId(), outMsgMemberSignList.getBrandId(), outMsgMemberSignList.getAlarmMobiles());


                    List<CpMsgMember> cpMsgMember = cpMsgMemberRepository.findByBrandId(outMsgMemberSignList.getBrandId());
                    if (null == cpMsgMember || 0 >= cpMsgMember.size()) {
                        paramMsg.put("resultCode", "fail");
                        paramMsg.put("resultMsg", "cpMsgMember表无相关数据");
                        return getGson().toJson(paramMsg);
                    }
                    paramMsg.put("cpMsgMember", cpMsgMember.get(0));
                    paramMsg.put("resultCode", "success");
                    cpMsgMemberSign.setSign(cpMsgMemberSign.getSign().replaceAll("【", "").replaceAll("】", ""));
                    paramMsg.put("cpMsgMemberSign", cpMsgMemberSign);
                    return getGson().toJson(paramMsg);
                }
            }
        } catch (Exception ex) {
            log.error("MsgAccountUpdateController.signUpdate" + ex.getMessage());
            ex.printStackTrace();
            Map<String, Object> resultMap = new HashMap<>();
            resultMap.put("resultCode", "fail");
            resultMap.put("resultMsg", "失败");
            return getGson().toJson(resultMap);
        }
    }
}
