package org.hshk.message;

import java.util.HashMap;
import java.util.Map;

/**
 * Created With IntelliJ IDEA
 * User: WangYong
 * Date: 2017/2/27 0027
 * Time: 08:58
 * ProjectName: msgCenterAPI
 * To change this template use File
 */
public class BaseController {
    //成功代码返回
    public static final String SUCCESS = "success";
    public static final String SUCCESS_MSG = "成功";
    //失败代码
    public static final String FAIL = "fail";
    public static final String FAIL_MSG = "失败";

    /**
     * 2017年1月18日 17:10:57
     * 执行成功返回
     *
     * @return
     */
    public static Map<String, Object> success() {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put("resultCode", SUCCESS);
        resultMap.put("resultMsg", SUCCESS_MSG);
        return resultMap;
    }

    /**
     * 2017年3月8日 14:14:42
     * 执行成功返回
     *
     * @param key
     * @param value
     * @return
     */
    public static Map<String, Object> success(String key, Object value) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put("resultCode", SUCCESS);
        resultMap.put("resultMsg", SUCCESS_MSG);
        resultMap.put(key, value);
        return resultMap;
    }

    /**
     * 2017年3月15日 15:10:43
     * 执行成功返回
     *
     * @param param
     * @return
     */
    public static Map<String, Object> success(Map<String, Object> param) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put("resultCode", SUCCESS);
        resultMap.put("resultMsg", SUCCESS_MSG);
        resultMap.putAll(param);
        return resultMap;
    }

    /**
     * 2017年1月18日 17:11:05
     * 执行失败返回
     *
     * @return
     */
    public static Map<String, Object> fail(String reason) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put("resultCode", FAIL);
        resultMap.put("resultMsg", FAIL_MSG);
        resultMap.put("resultReason", reason);
        return resultMap;
    }

    /**
     * 2017年3月8日 14:14:06
     * 执行失败返回
     *
     * @param key
     * @param value
     * @return
     */
    public static Map<String, Object> fail(String key, Object value) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put("resultCode", FAIL);
        resultMap.put("resultMsg", FAIL_MSG);
        resultMap.put(key, value);
        return resultMap;
    }

    /**
     * 2017年3月13日 16:44:47
     * 执行失败返回
     *
     * @param param
     * @return
     */
    public static Map<String, Object> fail(Map<String, Object> param) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put("resultCode", FAIL);
        resultMap.put("resultMsg", FAIL_MSG);
        resultMap.putAll(param);
        return resultMap;
    }
}