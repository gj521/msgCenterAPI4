package org.hshk.message.outDto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;

/**
 * Created by Administrator on 2017/3/28.
 */
@Api(value = "返回短信商户是否开通")
public class OutSmsOpen {

    @ApiModelProperty(name = "brandId", value = "品牌id")
    private int brandId;

    @ApiModelProperty(name = "open", value = "开通状态")
    private String open;

    @ApiModelProperty(name = "balanceCount", value = "剩余条数")
    private int balanceCount;

    @ApiModelProperty(name = "factoryId", value = "短信厂商id")
    private int factoryId;

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public String getOpen() {
        return open;
    }

    public void setOpen(String open) {
        this.open = open;
    }

    public int getBalanceCount() {
        return balanceCount;
    }

    public void setBalanceCount(int balanceCount) {
        this.balanceCount = balanceCount;
    }

    public int getFactoryId() {
        return factoryId;
    }

    public void setFactoryId(int factoryId) {
        this.factoryId = factoryId;
    }
}
