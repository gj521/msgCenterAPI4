package org.hshk.message.outDto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;

/**
 * Created by Administrator on 2017/3/31.
 */

//短信商家的信息类

@Api(value = "返回短信商户,修改短信厂家")
public class OutMsgMemberSignList {
    @ApiModelProperty(name = "id", value = "msg_member_sign 表的id")
    private int id;
    @ApiModelProperty(name = "orgId", value = "商户id")
    private int orgId ;

    @ApiModelProperty(name = "brandId", value = "品牌id")
    private int brandId ;

    @ApiModelProperty(name = "msgUsername", value = "短信账号")
    private String msgUsername ;

    @ApiModelProperty(name = "msgPassword", value = "短信密码")
    private String msgPassword ;

    @ApiModelProperty(name = "factoryId", value = "短信厂商")
    private int factoryId ;

    @ApiModelProperty(name = "payruleId", value = "规则id 大规则")
    private int payruleId ;

    @ApiModelProperty(name = "sign", value = "签名")
    private String sign ;

    @ApiModelProperty(name = "msgExtno", value = "短信通道光通互动才有")
    private String msgExtno ;

    @ApiModelProperty(name = "alarmMobiles", value = "报警联系人")
    private String alarmMobiles ;

    @ApiModelProperty(name = "isUsed", value = "是否在用enum('Yes','No')")
    private String isUsed ;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrgId() {
        return orgId;
    }

    public void setOrgId(int orgId) {
        this.orgId = orgId;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public String getMsgUsername() {
        return msgUsername;
    }

    public void setMsgUsername(String msgUsername) {
        this.msgUsername = msgUsername;
    }

    public String getMsgPassword() {
        return msgPassword;
    }

    public void setMsgPassword(String msgPassword) {
        this.msgPassword = msgPassword;
    }

    public int getFactoryId() {
        return factoryId;
    }

    public void setFactoryId(int factoryId) {
        this.factoryId = factoryId;
    }

    public int getPayruleId() {
        return payruleId;
    }

    public void setPayruleId(int payruleId) {
        this.payruleId = payruleId;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getMsgExtno() {
        return msgExtno;
    }

    public void setMsgExtno(String msgExtno) {
        this.msgExtno = msgExtno;
    }

    public String getAlarmMobiles() {
        return alarmMobiles;
    }

    public void setAlarmMobiles(String alarmMobiles) {
        this.alarmMobiles = alarmMobiles;
    }

    public String getIsUsed() {
        return isUsed;
    }

    public void setIsUsed(String isUsed) {
        this.isUsed = isUsed;
    }

    @Override
    public String toString() {
        return "OutMsgMemberSignList{" +
                "id=" + id +
                ", orgId=" + orgId +
                ", brandId=" + brandId +
                ", msgUsername='" + msgUsername + '\'' +
                ", msgPassword='" + msgPassword + '\'' +
                ", factoryId=" + factoryId +
                ", payruleId=" + payruleId +
                ", sign='" + sign + '\'' +
                ", msgExtno='" + msgExtno + '\'' +
                ", alarmMobiles='" + alarmMobiles + '\'' +
                ", isUsed='" + isUsed + '\'' +
                '}';
    }
}
