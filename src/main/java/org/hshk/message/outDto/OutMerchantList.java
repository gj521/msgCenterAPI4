package org.hshk.message.outDto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;

/**
 * Created by Administrator on 2017/3/28.
 */

//商户短信列表

@Api(value = "返回商户列表")
public class OutMerchantList {
    @ApiModelProperty(name = "id", value = "主键，唯一标识")
    private int id;// `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键，唯一标识 短信商家表',

    @ApiModelProperty(name = "orgId", value = "商户id")
    private int orgId;//`uid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商户id',

    @ApiModelProperty(name = "brandId", value = "品牌id")
    private int brandId;// `brand_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '品牌id',

    @ApiModelProperty(name = "factoryId", value = "短信厂商id")
    private int factoryId;

    @ApiModelProperty(name = "msgUsername", value = "短信账号")
    private String msgUsername;

    @ApiModelProperty(name = "factoryName", value = "短信厂商名称")
    private String factoryName;

    @ApiModelProperty(name = "payruleId", value = "充值规则id")
    private int payruleId;//  `payrule_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '充值规则id',s

    @ApiModelProperty(name = "totalCount", value = "充值总条数")
    private int totalCount;//`total_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '充值总条数',

    @ApiModelProperty(name = "balanceCount", value = "余额条数")
    private int balanceCount;//`balance_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '余额条数',

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrgId() {
        return orgId;
    }

    public void setOrgId(int orgId) {
        this.orgId = orgId;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public int getFactoryId() {
        return factoryId;
    }

    public void setFactoryId(int factoryId) {
        this.factoryId = factoryId;
    }

    public int getPayruleId() {
        return payruleId;
    }

    public void setPayruleId(int payruleId) {
        this.payruleId = payruleId;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getBalanceCount() {
        return balanceCount;
    }

    public void setBalanceCount(int balanceCount) {
        this.balanceCount = balanceCount;
    }

    public String getMsgUsername() {
        return msgUsername;
    }

    public void setMsgUsername(String msgUsername) {
        this.msgUsername = msgUsername;
    }

    public String getFactoryName() {
        return factoryName;
    }

    public void setFactoryName(String factoryName) {
        this.factoryName = factoryName;
    }
}
