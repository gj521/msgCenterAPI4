package org.hshk.message.outDto;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import org.hshk.message.entity.CpMsgPayDetail;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;

/**
 * Created by Administrator on 2017/3/31.
 */@Api(value = "短信申请记录")
public class OutMsgApplicationRecord{

    @ApiModelProperty(name = "id", value = "id")
    private int id;//`id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键，唯一标识 充值记录表',

    @ApiModelProperty(name = "uid", value = "商户id")
    private int uid;//`uid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商户id',

    @ApiModelProperty(name = "brandId", value = "品牌id")
    private int brandId;//`brand_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '品牌id',

    @ApiModelProperty(name = "payruleId", value = "充值规则id")
    private int payruleId;//  `payrule_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '充值规则id',

    @ApiModelProperty(name = "payCount", value = "充值数量")
    private int payCount;// `pay_count` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '充值数量',

    @ApiModelProperty(name = "payAmount", value = "购买金额")
    private BigDecimal payAmount;//  `pay_amount` decimal(9,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '购买金额',

    @ApiModelProperty(name = "orderNo", value = "支付订单号")
    private String orderNo;//  `order_no` varchar(50) NOT NULL DEFAULT '' COMMENT '支付订单号',

    @ApiModelProperty(name = "createDate", value = "创建时间")
    private int createDate;// `create_date` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',

    @ApiModelProperty(name = "updateDate", value = "最后修改时间")
    private int updateDate;//  `update_date` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后修改时间',


    @ApiModelProperty(name = "payType", value = "充值方式")
    private String payType;

    @ApiModelProperty(name = "status", value = "状态")
    private String status;

    @ApiModelProperty(name = "msgUserName", value = "短信账号")
    private String msgUsername;

    @ApiModelProperty(name = "factoryId", value = "短信厂商")
    private int factoryId;

    public int getFactoryId() {
        return factoryId;
    }

    public void setFactoryId(int factoryId) {
        this.factoryId = factoryId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public int getPayruleId() {
        return payruleId;
    }

    public void setPayruleId(int payruleId) {
        this.payruleId = payruleId;
    }

    public int getPayCount() {
        return payCount;
    }

    public void setPayCount(int payCount) {
        this.payCount = payCount;
    }

    public BigDecimal getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(BigDecimal payAmount) {
        this.payAmount = payAmount;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public int getCreateDate() {
        return createDate;
    }

    public void setCreateDate(int createDate) {
        this.createDate = createDate;
    }

    public int getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(int updateDate) {
        this.updateDate = updateDate;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsgUsername() {
        return msgUsername;
    }

    public void setMsgUsername(String msgUsername) {
        this.msgUsername = msgUsername;
    }

    @Override
    public String toString() {
        return "OutMsgApplicationRecord{" +
                "id=" + id +
                ", uid=" + uid +
                ", brandId=" + brandId +
                ", payruleId=" + payruleId +
                ", payCount=" + payCount +
                ", payAmount=" + payAmount +
                ", orderNo='" + orderNo + '\'' +
                ", createDate=" + createDate +
                ", updateDate=" + updateDate +
                ", payType='" + payType + '\'' +
                ", status='" + status + '\'' +
                ", msgUsername='" + msgUsername + '\'' +
                ", factoryId='" + factoryId + '\'' +
                '}';
    }
}
