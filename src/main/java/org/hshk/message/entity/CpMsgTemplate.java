package org.hshk.message.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

/**
 * Created With IntelliJ IDEA
 * User: WangYong
 * Date: 2017/2/24 0024
 * Time: 09:42
 * ProjectName: msgCenterAPI
 * To change this template use File
 */

//信息内容模版表

@ApiModel(value = "CpMsgTemplate")
@Entity
@Table(name = "msg_template")
public class CpMsgTemplate
{

    @ApiModelProperty(name = "id", value = "主键，唯一标识")
    @Id
    @GeneratedValue
    private int id;//int(11)  COMMENT '主键，唯一标识',

    @ApiModelProperty(name = "orgId", value = "商户编码")
    @Column(name = "uid", nullable = false)
    private int orgId;// int(11)  COMMENT '商户编码',

    @ApiModelProperty(name = "brandId", value = "品牌id")
    @Column(name = "brand_id", nullable = false)
    private int brandId;// int(11)  COMMENT '品牌id',

    @ApiModelProperty(name = "productType", value = "产品类型")
    @Column(name = "product_type", nullable = false)
    private String productType;// enum('HHY','HMD') COMMENT '产品类型（''门店'', ''好会员''）',

    @ApiModelProperty(name = "templateCode", value = "模版编码")
    @Column(name = "template_code", nullable = false)
    private String templateCode;// varchar(12)  COMMENT '模版编码',

    @ApiModelProperty(name = "msgContent", value = "模版内容")
    @Column(name = "msg_content", nullable = false)
    private String msgContent;// varchar(255)  COMMENT '模版内容',

    @ApiModelProperty(name = "createDate", value = "创建时间")
    @Column(name = "create_date", nullable = false)
    private int createDate;// int(10)  COMMENT '创建时间',

    @ApiModelProperty(name = "updateDate", value = "最后修改时间")
    @Column(name = "update_date", nullable = false)
    private int updateDate;// int(10)  COMMENT '最后修改时间',

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrgId() {
        return orgId;
    }

    public void setOrgId(int orgId) {
        this.orgId = orgId;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getTemplateCode() {
        return templateCode;
    }

    public void setTemplateCode(String templateCode) {
        this.templateCode = templateCode;
    }

    public String getMsgContent() {
        return msgContent;
    }

    public void setMsgContent(String msgContent) {
        this.msgContent = msgContent;
    }

    public int getCreateDate() {
        return createDate;
    }

    public void setCreateDate(int createDate) {
        this.createDate = createDate;
    }

    public int getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(int updateDate) {
        this.updateDate = updateDate;
    }
}
