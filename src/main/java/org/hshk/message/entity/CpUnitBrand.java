package org.hshk.message.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

/**
 * Created by Administrator on 2017/4/5.
 */

//商户的品牌相关的信息

@ApiModel(value = "CpUnitBrand")
@Entity
@Table(name = "cp_unit_brand")
public class CpUnitBrand {
    @ApiModelProperty(name = "id", value = "id  品牌表")
    @Id
    @GeneratedValue
    private int id;

    @ApiModelProperty(name = "orgId", value = "商户id")
    @Column(name = "org_id", nullable = false)
    private int orgId;

    @ApiModelProperty(name = "brandname", value = "品牌 就是这样数据库就这样")
    @Column(name = "brandname", nullable = false)
    private String brandname;

    @ApiModelProperty(name = "creatDt", value = "创建时间")
    @Column(name = "creat_dt", nullable = false)
    private int creatDt;

    @ApiModelProperty(name = "creatBy", value = "创建人")
    @Column(name = "creat_by", nullable = false)
    private String creatBy;

    @ApiModelProperty(name = "lastupdateDt", value = "最近一次修改时间")
    @Column(name = "lastupdate_dt", nullable = false)
    private int lastupdateDt;

    @ApiModelProperty(name = "lastupdateBy", value = "修改人")
    @Column(name = "lastupdate_by", nullable = false)
    private String lastupdateBy;

    @ApiModelProperty(name = "status", value = "状态 'yes','no','delete'")
    @Column(name = "status", nullable = false)
    private String status;

    @ApiModelProperty(name = "remark", value = "备注")
    @Column(name = "remark", nullable = false)
    private String remark;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrgId() {
        return orgId;
    }

    public void setOrgId(int orgId) {
        this.orgId = orgId;
    }

    public String getBrandname() {
        return brandname;
    }

    public void setBrandname(String brandname) {
        this.brandname = brandname;
    }

    public int getCreatDt() {
        return creatDt;
    }

    public void setCreatDt(int creatDt) {
        this.creatDt = creatDt;
    }

    public String getCreatBy() {
        return creatBy;
    }

    public void setCreatBy(String creatBy) {
        this.creatBy = creatBy;
    }

    public int getLastupdateDt() {
        return lastupdateDt;
    }

    public void setLastupdateDt(int lastupdateDt) {
        this.lastupdateDt = lastupdateDt;
    }

    public String getLastupdateBy() {
        return lastupdateBy;
    }

    public void setLastupdateBy(String lastupdateBy) {
        this.lastupdateBy = lastupdateBy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "CpUnitBrand{" +
                "id=" + id +
                ", orgId=" + orgId +
                ", brandname=" + brandname +
                ", creatDt=" + creatDt +
                ", creatBy=" + creatBy +
                ", lastupdateDt=" + lastupdateDt +
                ", lastupdateBy=" + lastupdateBy +
                ", status=" + status +
                ", remark=" + remark +
                '}';
    }
}
