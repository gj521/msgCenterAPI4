package org.hshk.message.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by Administrator on 2017/3/24.
 */


//短信支付规则详细描述

@ApiModel(value = "CpMsgPayruleDetail")
@Entity
@Table(name = "msg_payrule_detail")
public class CpMsgPayruleDetail {

    @ApiModelProperty(name = "id", value = "主键，唯一标识 ")
    @Id
    @GeneratedValue
    private int id;//  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键，唯一标识 具体规则表',

    @ApiModelProperty(name = "payruleId", value = "充值规则id")
    @Column(name = "payrule_id", nullable = false)
    private int payruleId;//`payrule_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '充值规则id',

    @ApiModelProperty(name = "count", value = "数量")
    @Column(name = "count", nullable = false)
    private int count;//`count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '数量',

    @ApiModelProperty(name = "amount", value = "金额")
    @Column(name = "amount", nullable = false)
    private BigDecimal amount;//`amount` decimal(9,2) unsigned NOT NULL DEFAULT '0.00' COMMENT '金额',

    @ApiModelProperty(name = "createDate", value = "创建时间")
    @Column(name = "create_date", nullable = false)
    private int createDate;// `create_date` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',

    @ApiModelProperty(name = "updateDate", value = "最后修改时间")
    @Column(name = "update_date", nullable = false)
    private int updateDate;// `update_date` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后修改时间',

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPayruleId() {
        return payruleId;
    }

    public void setPayruleId(int payruleId) {
        this.payruleId = payruleId;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public int getCreateDate() {
        return createDate;
    }

    public void setCreateDate(int createDate) {
        this.createDate = createDate;
    }

    public int getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(int updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public String toString() {
        return "CpMsgPayruleDetail{" +
                "id=" + id +
                ", payruleId=" + payruleId +
                ", count=" + count +
                ", amount=" + amount +
                ", createDate=" + createDate +
                ", updateDate=" + updateDate +
                '}';
    }
}
