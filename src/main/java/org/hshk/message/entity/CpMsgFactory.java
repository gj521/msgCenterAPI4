package org.hshk.message.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

/**
 * Created With IntelliJ IDEA
 * User: WangYong
 * Date: 2017/2/24 0024
 * Time: 17:06
 * ProjectName: msgCenterAPI
 * To change this template use File
 */

//短信厂家表


@ApiModel(value = "CpMsgFactory")
@Entity
@Table(name = "msg_factory")
public class CpMsgFactory
{
    @ApiModelProperty(name = "id", value = "主键标识")
    @Id
    @GeneratedValue
    private int id;//int(11) COMMENT '主键，唯一标识',

    @ApiModelProperty(name = "factoryName", value = "短信厂家名称")
    @Column(name = "factory_name", nullable = false)
    private String factoryName;//varchar(100) COMMENT '短信厂家名称',

    @ApiModelProperty(name = "createDate", value = "创建时间")
    @Column(name = "create_date", nullable = false)
    private int createDate;//int(10) COMMENT '创建时间',

    @ApiModelProperty(name = "updateDate", value = "最后修改时间")
    @Column(name = "update_date", nullable = false)
    private int updateDate;//int(10) COMMENT '最后修改时间'

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFactoryName() {
        return factoryName;
    }

    public void setFactoryName(String factoryName) {
        this.factoryName = factoryName;
    }

    public int getCreateDate() {
        return createDate;
    }

    public void setCreateDate(int createDate) {
        this.createDate = createDate;
    }

    public int getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(int updateDate) {
        this.updateDate = updateDate;
    }
}
