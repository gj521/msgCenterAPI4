package org.hshk.message.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

/**
 * Created With IntelliJ IDEA
 * User: WangYong
 * Date: 2017/2/24 0024
 * Time: 17:06
 * ProjectName: msgCenterAPI
 * To change this template use File
 */

//用户商家表

@ApiModel(value = "CpMsgMemberSign")
@Entity
@Table(name = "msg_member_sign")
public class CpMsgMemberSign
{
    @ApiModelProperty(name = "id", value = "主键标识")
    @Id
    @GeneratedValue
    private int id;//int(11) NOT NULL AUTO_INCREMENT COMMENT '主键，唯一标识',

    @ApiModelProperty(name = "orgId", value = "商户id")
    @Column(name = "uid", nullable = false)
    private int orgId;//int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商户id',

    @ApiModelProperty(name = "brandId", value = "品牌id")
    @Column(name = "brand_id", nullable = false)
    private int brandId;//int(11) unsigned NOT NULL DEFAULT '0' COMMENT '品牌id',

    @ApiModelProperty(name = "sign", value = "签名")
    @Column(name = "sign", nullable = false)
    private String sign;//varchar(50) NOT NULL DEFAULT '' COMMENT '签名',

    @ApiModelProperty(name = "factoryId", value = "短信厂家id")
    @Column(name = "factory_id", nullable = false)
    private int factoryId;//int(11) unsigned NOT NULL DEFAULT '0' COMMENT '短信厂家表主键',

    @ApiModelProperty(name = "msgUsername", value = "短信账户")
    @Column(name = "msg_username", nullable = false)
    private String msgUsername;//varchar(50) NOT NULL DEFAULT '' COMMENT '短信平台用户名',

    @ApiModelProperty(name = "msgPassword", value = "短信密码")
    @Column(name = "msg_password", nullable = false)
    private String msgPassword;//varchar(50) NOT NULL DEFAULT '' COMMENT '短信平台密码',

    @ApiModelProperty(name = "msgExtno", value = "短信通道")
    @Column(name = "msg_extno", nullable = false)
    private String msgExtno;//varchar(20) NOT NULL DEFAULT '' COMMENT 'EXTNO',

    @ApiModelProperty(name = "isUsed", value = "是否在用")
    @Column(name = "is_used", nullable = false)
    private String isUsed;//enum('Yes','No') NOT NULL DEFAULT 'No' COMMENT '是否在用',

    @ApiModelProperty(name = "createDate", value = "创建时间")
    @Column(name = "create_date", nullable = false)
    private int createDate;//int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',

    @ApiModelProperty(name = "updateDate", value = "更新时间")
    @Column(name = "update_date", nullable = false)
    private int updateDate;//int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后修改时间',

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrgId() {
        return orgId;
    }

    public void setOrgId(int orgId) {
        this.orgId = orgId;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public int getFactoryId() {
        return factoryId;
    }

    public void setFactoryId(int factoryId) {
        this.factoryId = factoryId;
    }

    public String getMsgUsername() {
        return msgUsername;
    }

    public void setMsgUsername(String msgUsername) {
        this.msgUsername = msgUsername;
    }

    public String getMsgPassword() {
        return msgPassword;
    }

    public void setMsgPassword(String msgPassword) {
        this.msgPassword = msgPassword;
    }

    public String getMsgExtno() {
        return msgExtno;
    }

    public void setMsgExtno(String msgExtno) {
        this.msgExtno = msgExtno;
    }

    public String getIsUsed() {
        return isUsed;
    }

    public void setIsUsed(String isUsed) {
        this.isUsed = isUsed;
    }

    public int getCreateDate() {
        return createDate;
    }

    public void setCreateDate(int createDate) {
        this.createDate = createDate;
    }

    public int getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(int updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public String toString() {
        return "CpMsgMemberSign{" +
                "id=" + id +
                ", orgId=" + orgId +
                ", brandId=" + brandId +
                ", sign='" + sign + '\'' +
                ", factoryId=" + factoryId +
                ", msgUsername='" + msgUsername + '\'' +
                ", msgPassword='" + msgPassword + '\'' +
                ", msgExtno='" + msgExtno + '\'' +
                ", isUsed='" + isUsed + '\'' +
                ", createDate=" + createDate +
                ", updateDate=" + updateDate +
                '}';
    }
}