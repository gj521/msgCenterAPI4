package org.hshk.message.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Administrator on 2017/3/24.
 */


//短信的充值规则（规则）

@ApiModel(value = "CpMsgPayrule")
@Entity
@Table(name = "msg_payrule")
public class CpMsgPayrule {

    @ApiModelProperty(name = "", value = "")
    @Id
    @GeneratedValue
    private int id;//  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键，唯一标识 规则表',

    @ApiModelProperty(name = "payruleName", value = "充值规则名称")
    @Column(name = "payrule_name", nullable = false)
    private String payruleName;//`payrule_name` varchar(100) NOT NULL DEFAULT '' COMMENT '充值规则名称',

    @ApiModelProperty(name = "beginExpiryDate", value = "开始有效期")
    @Column(name = "begin_expiry_date", nullable = false)
    private int beginExpiryDate;//  `begin_expiry_date` date NOT NULL COMMENT '开始有效期',

    @ApiModelProperty(name = "endExpiryDate", value = "结束有效期")
    @Column(name = "end_expiry_date", nullable = false)
    private int endExpiryDate;//`end_expiry_date` date NOT NULL COMMENT '结束有效期',

    @ApiModelProperty(name = "createDate", value = "创建时间")
    @Column(name = "create_date", nullable = false)
    private int createDate;//`create_date` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',

    @ApiModelProperty(name = "updateDate", value = "最后修改时间")
    @Column(name = "update_date", nullable = false)
    private int updateDate;//  `update_date` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后修改时间',

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPayruleName() {
        return payruleName;
    }

    public void setPayruleName(String payruleName) {
        this.payruleName = payruleName;
    }

    public int getBeginExpiryDate() {
        return beginExpiryDate;
    }

    public void setBeginExpiryDate(int beginExpiryDate) {
        this.beginExpiryDate = beginExpiryDate;
    }

    public int getEndExpiryDate() {
        return endExpiryDate;
    }

    public void setEndExpiryDate(int endExpiryDate) {
        this.endExpiryDate = endExpiryDate;
    }

    public int getCreateDate() {
        return createDate;
    }

    public void setCreateDate(int createDate) {
        this.createDate = createDate;
    }

    public int getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(int updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public String toString() {
        return "CpMsgPayrule{" +
                "id=" + id +
                ", payruleName=" + payruleName +
                ", beginExpiryDate=" + beginExpiryDate +
                ", endExpiryDate=" + endExpiryDate +
                ", createDate=" + createDate +
                ", updateDate=" + updateDate +
                '}';
    }
}
