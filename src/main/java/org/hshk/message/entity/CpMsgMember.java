package org.hshk.message.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

/**
 * Created by Administrator on 2017/3/24.
 */


//商户充值类（会员）

@ApiModel(value = "CpMsgMember")
@Entity
@Table(name = "msg_member")
public class CpMsgMember {

    @ApiModelProperty(name = "id", value = "主键，唯一标识")
    @Id
    @GeneratedValue
    private int id;// `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键，唯一标识 短信商家表',

    @ApiModelProperty(name = "orgId", value = "商户id")
    @Column(name = "uid", nullable = false)
    private int orgId;//`uid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '商户id',

    @ApiModelProperty(name = "brandId", value = "品牌id")
    @Column(name = "brand_id", nullable = false)
    private int brandId;// `brand_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '品牌id',

    @ApiModelProperty(name = "payruleId", value = "充值规则id")
    @Column(name = "payrule_id", nullable = false)
    private int payruleId;//  `payrule_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '充值规则id',s

    @ApiModelProperty(name = "totalCount", value = "充值总条数")
    @Column(name = "total_count", nullable = false)
    private int totalCount;//`total_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '充值总条数',

    @ApiModelProperty(name = "balanceCount", value = "余额条数")
    @Column(name = "balance_count", nullable = false)
    private int balanceCount;//`balance_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '余额条数',

    @ApiModelProperty(name = "alarmMobiles", value = "余额报警联系人")
    @Column(name = "alarm_mobiles", nullable = false)
    private String alarmMobiles;//`alarm_mobiles` varchar(255) NOT NULL DEFAULT '' COMMENT '余额报警联系人',

    @ApiModelProperty(name = "createDate", value = "创建时间")
    @Column(name = "create_date", nullable = false)
    private int createDate;// `create_date` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',

    @ApiModelProperty(name = "updateDate", value = "最后修改时间")
    @Column(name = "update_date", nullable = false)
    private int updateDate;// `update_date` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后修改时间',

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrgId() {
        return orgId;
    }

    public void setOrgId(int orgId) {
        this.orgId = orgId;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public int getPayruleId() {
        return payruleId;
    }

    public void setPayruleId(int payruleId) {
        this.payruleId = payruleId;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public int getBalanceCount() {
        return balanceCount;
    }

    public void setBalanceCount(int balanceCount) {
        this.balanceCount = balanceCount;
    }

    public String getAlarmMobiles() {
        return alarmMobiles;
    }

    public void setAlarmMobiles(String alarmMobiles) {
        this.alarmMobiles = alarmMobiles;
    }

    public int getCreateDate() {
        return createDate;
    }

    public void setCreateDate(int createDate) {
        this.createDate = createDate;
    }

    public int getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(int updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public String toString() {
        return "CpMsgMember{" +
                "id=" + id +
                ", orgId=" + orgId +
                ", brandId=" + brandId +
                ", payruleId=" + payruleId +
                ", totalCount=" + totalCount +
                ", balanceCount=" + balanceCount +
                ", alarmMobiles='" + alarmMobiles + '\'' +
                ", createDate=" + createDate +
                ", updateDate=" + updateDate +
                '}';
    }
}
