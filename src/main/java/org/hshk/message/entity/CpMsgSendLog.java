package org.hshk.message.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

/**
 * Created With IntelliJ IDEA
 * User: WangYong
 * Date: 2017/2/24 0024
 * Time: 17:07
 * ProjectName: msgCenterAPI
 * To change this template use File
 */

//信息发送日志表

@ApiModel(value = "CpMsgSendLog")
@Entity
@Table(name = "msg_send_log")
public class CpMsgSendLog
{
    @ApiModelProperty(name = "id", value = "id，唯一标识")
    @Id
    @GeneratedValue
    private int id;//int(11)AUTO_INCREMENT,

    @ApiModelProperty(name = "orgId", value = "商户id")
    @Column(name = "uid", nullable = false)
    private int orgId;//int(11) COMMENT '商户id',

    @ApiModelProperty(name = "mobile", value = "手机号")
    @Column(name = "mobile", nullable = false)
    private String mobile;//varchar(20) COMMENT '手机号',

    @ApiModelProperty(name = "content", value = "短信发送内容")
    @Column(name = "content", nullable = false)
    private String content;//varchar(4096) COMMENT '短信发送内容',

    @ApiModelProperty(name = "statusCode", value = "短信发送状态码，基于短信运营商的返回值")
    @Column(name = "status_code", nullable = false)
    private String statusCode;//varchar(10) COMMENT '短信发送状态码，基于短信运营商的返回值',

    @ApiModelProperty(name = "statusDesc", value = "短信发送状态描述")
    @Column(name = "status_desc", nullable = false)
    private String statusDesc;//varchar(255) COMMENT '短信发送状态描述',

    @ApiModelProperty(name = "type", value = "验证类短信")
    @Column(name = "type", nullable = false)
    private String type;//enum('MARKETING','VERIFY') COMMENT '短信类型，VERIFY：验证类短信；MARKETING：营销类短信',

    @ApiModelProperty(name = "status", value = "状态")
    @Column(name = "status", nullable = false)
    private String status;//enum('SEND_FAIL','SEND_SUCCESS','WAIT')

    @ApiModelProperty(name = "createDate", value = "创建时间")
    @Column(name = "create_date", nullable = false)
    private int createDate;//int(10) COMMENT '创建时间',

    @ApiModelProperty(name = "updateDate", value = "最后修改时间")
    @Column(name = "update_date", nullable = false)
    private int updateDate;//int(10) COMMENT '最后修改时间',

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrgId() {
        return orgId;
    }

    public void setOrgId(int orgId) {
        this.orgId = orgId;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCreateDate() {
        return createDate;
    }

    public void setCreateDate(int createDate) {
        this.createDate = createDate;
    }

    public int getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(int updateDate) {
        this.updateDate = updateDate;
    }
}
