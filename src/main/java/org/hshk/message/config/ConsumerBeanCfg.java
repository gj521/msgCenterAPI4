package org.hshk.message.config;

import com.aliyun.openservices.ons.api.MessageListener;
import com.aliyun.openservices.ons.api.bean.ConsumerBean;
import com.aliyun.openservices.ons.api.bean.Subscription;
import org.hshk.message.mq.MQConsumer;
import org.hshk.message.service.CpMsgMemberSignService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * 2017年02月25日
 * 23:58
 * Create By WangYong's Intellij IDEA
 */
@Configuration
@RefreshScope
public class ConsumerBeanCfg
{
    
    @Value("${MQ_TOPIC_CID}")
    private String mqTopicCid;
    @Value("${MQ_TOPIC_ACCESS_KEY}")
    private String mqTopicAccessKey;
    @Value("${MQ_TOPIC_ACCESS_SECURET}")
    private String mqTopicAccessSecuret;
    @Value("${MQ_TOPIC_ID}")
    private String mqTopicId;

    /**
     * 2017年2月26日 00:30:11
     * 创建consumerBean对象
     * @return
     */
    @Bean(initMethod = "start", destroyMethod = "shutdown")
    public ConsumerBean createConsumerBean(MQConsumer mqConsumer, Subscription subscription)
    {
        System.out.println("**************************************************初始化队列消费者开始**************************************************");
        ConsumerBean consumerBean = new ConsumerBean();
        System.out.println("mqTopicCid : " + mqTopicCid + "\nmqTopicId : " + mqTopicId + "\nMqTopicAccessKey : " + mqTopicAccessKey
                + "\nMqTopicAccessSecuret : " + mqTopicAccessSecuret);
        Properties properties = new Properties();
        properties.setProperty("ConsumerId", mqTopicCid);
        properties.setProperty("AccessKey", mqTopicAccessKey);
        properties.setProperty("SecretKey", mqTopicAccessSecuret);
        consumerBean.setProperties(properties);
        Map<Subscription, MessageListener> subscriptionMessageListenerMap = 
                new HashMap<Subscription, MessageListener>();
//        Subscription subscription = new Subscription();
        subscription.setExpression("*");
        subscription.setTopic(mqTopicId);
        subscriptionMessageListenerMap.put(subscription, mqConsumer);
        System.out.println("subscription.getTopic() : " + subscription.getTopic());
        consumerBean.setSubscriptionTable(subscriptionMessageListenerMap);
        System.out.println("**************************************************初始化队列消费者开始**************************************************");
        return consumerBean;
    }

    /**
     * 2017年2月26日 21:19:20
     * 创建线程消费对象
     * @return
     */
    @Bean
    public MQConsumer createMQConsumer()
    {
        return new MQConsumer();
    }
    
    @Bean
    public Subscription createSubscription()
    {
        return new Subscription();
    }
    
}