package org.hshk.message.config;

import com.aliyun.openservices.ons.api.Producer;
import com.aliyun.openservices.ons.api.PropertyKeyConst;
import com.aliyun.openservices.ons.api.bean.ProducerBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

/**
 * 2017年02月25日
 * 23:42
 * Create By WangYong's Intellij IDEA
 */
@Configuration
@ComponentScan("com.aliyun.openservices.ons.api.bean")
@RefreshScope
public class ProducerBeanCfg
{
    @Value("${MQ_TOPIC_PID}")
    public String mqTopicPid;
    @Value("${MQ_TOPIC_ACCESS_KEY}")
    public String mqTopicAccessKey;
    @Value("${MQ_TOPIC_ACCESS_SECURET}")
    public String mqTopicAccessSecuret;
    
    @Bean(initMethod = "start", destroyMethod = "shutdown")
    public ProducerBean createProducer()
    {
        System.out.println("*****************************************************初始化队列生产者开始*****************************************************");
        ProducerBean bean = new ProducerBean();
        Properties properties = new Properties();
        properties.setProperty("ProducerId", mqTopicPid);
        properties.setProperty("AccessKey", mqTopicAccessKey);
        properties.setProperty("SecretKey", mqTopicAccessSecuret);
        System.out.println("MqTopicPid : " + mqTopicPid + "\nMqTopicAccessKey : " + mqTopicAccessKey 
                + "\nMqTopicAccessSecuret : " + mqTopicAccessSecuret);
        bean.setProperties(properties);
        System.out.println("*****************************************************初始化队列生产者结束*****************************************************");
        return bean;
    }
}