package org.hshk.message.utils;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.hshk.message.entity.CpMsgSendLog;
import org.hshk.message.service.CpMsgSendLogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 2017年02月24日
 * 22:06
 * Create By WangYong's Intellij IDEA
 */
@Component
public class GthdMessageImpl implements IMessage {
    //日志记录类
    private static final Logger log = LoggerFactory.getLogger(GthdMessageImpl.class);
    @Autowired
    private CpMsgSendLogService cpMsgSendLogService;//日志对象存储
    //短信厂家请求URL
    private static final String URL = "http://ganiu.guangtongcloud.com:7862/sms";

    /**
     * 2017年2月24日 22:13:27
     * 光通互动发送短信实现
     *
     * @param brandId
     * @param content
     * @param username
     * @param password
     * @param mobile
     * @return
     */
    @Override
    public String send(int brandId, String content, String username,
                       String password, String mobile, String extno) {
        //当前系统时间
        int dateTime = (int) (System.currentTimeMillis() / 1000);
        try {
            log.info("GthdMessageImpl.send [ brandId=" + brandId + ", content=" + content
                    + ", username=" + username + ", password=" + password + ", mobile="
                    + mobile + " ]");
            HttpClient client = HttpClientFactory.getInstance();
            PostMethod method = new PostMethod(URL);
            client.getParams().setContentCharset("UTF-8");
            method.setRequestHeader("ContentType", "application/x-www-form-urlencoded;charset=UTF-8");
            NameValuePair[] data = {//提交短信
                    new NameValuePair("action", "send"),
                    new NameValuePair("account", username),
                    new NameValuePair("password", password),
                    new NameValuePair("mobile", mobile),
                    new NameValuePair("content", content),
                    new NameValuePair("extno", extno),//此处需要修改的部分
            };
            method.setRequestBody(data);
            client.executeMethod(method);
            String SubmitResult = method.getResponseBodyAsString();
            Document doc = DocumentHelper.parseText(SubmitResult);
            Element root = doc.getRootElement();
            //日志记录类
            CpMsgSendLog sendLog = new CpMsgSendLog();
            sendLog.setOrgId(brandId);
            sendLog.setContent(content);
            sendLog.setCreateDate(dateTime);
            sendLog.setUpdateDate(dateTime);
            sendLog.setMobile(mobile);
            sendLog.setStatusCode(root.elementText("returnstatus").trim());
            sendLog.setStatusDesc(root.elementText("message").trim());
            sendLog.setType("VERIFY");
            System.out.println(root.getText());

            if ("Success".equals(root.elementText("returnstatus").trim())) {
                sendLog.setStatus("SEND_SUCCESS");
            } else {
                Element secRoot = doc.getRootElement().element("resplist");
                String resultMsg = secRoot.elementText("resp").trim();
                String code = resultMsg.substring(resultMsg.lastIndexOf("#@#") + 3, resultMsg.length());
                if ("15".equals(code)) {
                    //切换短信平台
                    cpMsgSendLogService.changeMsgPlatForm(brandId);
                }
                sendLog.setStatus("SEND_FAIL");
            }
            //保存短信发送记录
            cpMsgSendLogService.save(sendLog);
            return root.elementText("returnstatus");
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error("GthdMessageImpl.send [ brandId=" + brandId + ", content=" + content
                    + ", username=" + username + ", password=" + password + ", mobile="
                    + mobile + " ]");
            return "";
        }
    }

    /**
     * 2017年2月24日 22:13:44
     * 光通互动短信余额查询实现
     *
     * @param brandId
     * @param username
     * @param password
     * @return
     */
    @Override
    public int overage(int brandId, String username, String password) {
        //当前系统时间
        int dateTime = (int) (System.currentTimeMillis() / 1000);
        try {
            //查询余额接口
            HttpClient client = HttpClientFactory.getInstance();
            //POST方式发送
            PostMethod method = new PostMethod(URL);
            method.setRequestHeader("ContentType", "application/x-www-form-urlencoded;charset=UTF-8");
            client.getParams().setContentCharset("UTF-8");
            NameValuePair[] data = {//查询短信参数
                    new NameValuePair("action", "overage"),
                    new NameValuePair("account", username),
                    new NameValuePair("password", password),
            };
            method.setRequestBody(data);
            client.executeMethod(method);
            String SubmitResult = method.getResponseBodyAsString();
            Document doc = DocumentHelper.parseText(SubmitResult);
            Element root = doc.getRootElement();
            //保存日志记录
            if (!"Sucess".equals(root.elementText("returnstatus").trim())) {
                CpMsgSendLog sendLog = new CpMsgSendLog();
                sendLog.setOrgId(brandId);//uid即bradnId
                sendLog.setContent("查询余额");//content
                sendLog.setCreateDate(dateTime);//create_date
                sendLog.setUpdateDate(dateTime);//update_date
                sendLog.setMobile("0");//mobile
                sendLog.setStatusCode(root.elementText("returnstatus").trim());//status_code：发送结果编码
                sendLog.setStatusDesc(root.elementText("message").trim());//status_desc：发送结果描述
                sendLog.setType("VERIFY");//type：营销类和验证类
                sendLog.setStatus("SEND_FAIL");
                cpMsgSendLogService.save(sendLog);
            }

            //返回查询到的余额
            if (null != root.elementText("overage") && !"".equals(root.elementText("overage"))) {
                int result = (int) (Integer.parseInt(root.elementText("overage")) / 33);
                return result;
            } else {
                return 0;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error("GthdMessageImpl.overage [ brandId=" + brandId + ", username=" + username
                    + ", password=" + password + " ]");
            return -1;
        }
    }
}