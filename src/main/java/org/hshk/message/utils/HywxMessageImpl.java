package org.hshk.message.utils;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.hshk.message.entity.CpMsgSendLog;
import org.hshk.message.service.CpMsgSendLogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 2017年02月24日
 * 22:06
 * Create By WangYong's Intellij IDEA
 */
@Component
public class HywxMessageImpl implements IMessage {
    //日志记录类
    private static final Logger log = LoggerFactory.getLogger(HywxMessageImpl.class);
    @Autowired
    private CpMsgSendLogService cpMsgSendLogService;//日志对象存储

    /**
     * 2017年2月24日 22:13:27
     * 互亿无线发送短信实现
     *
     * @param brandId
     * @param content
     * @param username
     * @param password
     * @param mobile
     * @return
     */
    @Override
    public String send(int brandId, String content, String username,
                       String password, String mobile, String extno) {
        //当前系统时间
        int dateTime = (int) (System.currentTimeMillis() / 1000);
        try {
            HttpClient client = HttpClientFactory.getInstance();
            PostMethod method = new PostMethod("http://121.199.16.178/webservice/sms.php?method=Submit");
            client.getParams().setContentCharset("UTF-8");
            method.setRequestHeader("ContentType", "application/x-www-form-urlencoded;charset=UTF-8");
            NameValuePair[] data = {//提交短信
                    new NameValuePair("account", username),
                    new NameValuePair("password", password), //密码可以使用明文密码或使用32位MD5加密
                    new NameValuePair("mobile", mobile),
                    new NameValuePair("content", content),
            };
            method.setRequestBody(data);
            client.executeMethod(method);
            String SubmitResult = method.getResponseBodyAsString();
            Document doc = DocumentHelper.parseText(SubmitResult);
            Element root = doc.getRootElement();
            //日志记录类
            CpMsgSendLog sendLog = new CpMsgSendLog();
            sendLog.setOrgId(brandId);
            sendLog.setContent(content);
            sendLog.setCreateDate(dateTime);
            sendLog.setUpdateDate(dateTime);
            sendLog.setMobile(mobile);
            sendLog.setStatusCode(root.elementText("code").trim());
            sendLog.setStatusDesc(root.elementText("msg").trim());
            sendLog.setType("VERIFY");
            if ("2".equals(root.elementText("code").trim())) {
                sendLog.setStatus("SEND_SUCCESS");                        //status
            } else {
                if ("4051".equals(root.elementText("code").trim())) {
                    cpMsgSendLogService.changeMsgPlatForm(brandId);
                }
                sendLog.setStatus("SEND_FAIL");
            }
            cpMsgSendLogService.save(sendLog);
            return root.elementText("code");
        } catch (Exception ex) {
            ex.printStackTrace();
            log.error("HywxMessageImpl.send [ brandId=" + brandId + ", content=" + content
                    + ", username=" + username + ", password=" + password + ", mobile="
                    + mobile + " ]");
            return "0";
        }
    }

    /**
     * 2017年2月24日 22:13:44
     * 互亿无线短信余额查询实现
     *
     * @param brandId
     * @param username
     * @param password
     * @return
     */
    @Override
    public int overage(int brandId, String username, String password) {
        //当前系统时间
        int nowDate = (int) (System.currentTimeMillis() / 1000);
        try {
            //模拟HTTP请求
            HttpClient client = HttpClientFactory.getInstance();
            PostMethod method = new PostMethod("http://121.199.16.178/webservice/sms.php?method=GetNum");
            client.getParams().setContentCharset("UTF-8");
            method.setRequestHeader("ContentType", "application/x-www-form-urlencoded;charset=UTF-8");
            NameValuePair[] data = {//提交短信
                    new NameValuePair("account", username),
                    new NameValuePair("password", password), //密码可以使用明文密码或使用32位MD5加密
            };
            method.setRequestBody(data);
            client.executeMethod(method);
            String SubmitResult = method.getResponseBodyAsString();
            Document doc = DocumentHelper.parseText(SubmitResult);
            Element root = doc.getRootElement();
            if (!"2".equals(root.elementText("code").trim())) {
                CpMsgSendLog sendLog = new CpMsgSendLog();
                sendLog.setOrgId(brandId);//uid即bradnId
                sendLog.setContent("查询余额");//content
                sendLog.setCreateDate(nowDate);//create_date
                sendLog.setUpdateDate(nowDate);//update_date
                sendLog.setMobile("0");//mobile
                sendLog.setStatusCode(root.elementText("code").trim());//status_code：短信发送的结果
                sendLog.setStatusDesc(root.elementText("msg").trim());//status_desc
                sendLog.setType("VERIFY");//type类型：营销类和验证类
                sendLog.setStatus("SEND_FAIL");
                cpMsgSendLogService.save(sendLog);
            }
            return Integer.valueOf(root.elementText("num"));

        } catch (Exception ex) {
            ex.printStackTrace();
            log.error("HywxMessageImpl.overage [ brandId=" + brandId + ", username=" + username
                    + ", password=" + password + " ]");
            return -1;
        }
    }
}