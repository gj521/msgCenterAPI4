package org.hshk.message.utils;

/**
 * 2017年02月24日
 * 22:07
 * Create By WangYong's Intellij IDEA
 */
public interface IMessage
{
    /**
     * 2017年2月24日 22:08:53
     * 短信发送接口，由每个短信厂家具体实现
     * @param brandId
     * @param content
     * @param username
     * @param password
     * @param mobile
     * @param extno
     * @return
     */
    String send(int brandId, String content, String username, 
                String password, String mobile, String extno);

    /**
     * 2017年2月24日 22:09:51
     * 短信余额查询接口，由每个短信厂家具体实现
     * @param brandId
     * @param username
     * @param password
     * @return
     */
    int overage(int brandId, String username, String password);
}
