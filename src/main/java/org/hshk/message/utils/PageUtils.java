package org.hshk.message.utils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Created With IntelliJ IDEA
 * User: WangYong
 * Date: 2017/3/1 0001
 * Time: 23:08
 * ProjectName: msgCenterAPI
 * To change this template use File
 */
@ApiModel(value = "PageUtils")
public class PageUtils
{
    @ApiModelProperty(name = "pageSize", value = "每页多少行")
    private int pageSize;//每页多少行

    @ApiModelProperty(name = "pages", value = "共多少页")
    private int pages;//共多少页

    @ApiModelProperty(name = "currentPage", value = "当前页码")
    private int currentPage;//当前页码

    @ApiModelProperty(name = "totalSize", value = "共多少条")
    private int totalSize;//共多少条
    
    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public int getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(int totalSize) {
        this.totalSize = totalSize;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    @Override
    public String toString()
    {
        return "PageUtils [ pages=" + pages + ", pageSize=" + pageSize + " ]";
    }
}
