package org.hshk.message.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created With IntelliJ IDEA
 * User: WangYong
 * Date: 2016/12/22 0022
 * Time: 15:26
 * ProjectName: spring-boot-2-1
 * To change this template use File
 */
public class MobileMatcher 
{

    /**
     * 验证手机号是否符合规则。如果不符合规则，就不发送
     * @param telephoneNumber
     * @return
     */
    public static boolean match(String telephoneNumber)
    {
        Pattern p = Pattern.compile("^1[34578]{1}[0-9]{1}[0-9]{8}$");
        Matcher match = p.matcher(telephoneNumber);
        return match.matches();
    }
}
