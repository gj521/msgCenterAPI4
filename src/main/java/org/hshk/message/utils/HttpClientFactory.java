package org.hshk.message.utils;

import org.apache.commons.httpclient.HttpClient;

/**
 * 2017年02月24日
 * 22:32
 * Create By WangYong's Intellij IDEA
 */
public class HttpClientFactory
{
    //单实例操作对象
    private static HttpClient client = new HttpClient();
    //获取HttpClient对象实例
    public static HttpClient getInstance()
    {
        if(null == client)
        {
            return new HttpClient();
        }else
        {
            return client;
        }
    }
}
