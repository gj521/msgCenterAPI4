package org.hshk.message.admin;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created With IntelliJ IDEA
 * User: WangYong
 * Date: 2017/3/1 0001
 * Time: 14:21
 * ProjectName: msgCenterAPI
 * To change this template use File
 */
@Controller
@RequestMapping(value = "/logs")
public class AdminLogsController
{
    
    @RequestMapping(value = "/index")
    public String index(Model model)
    {
        try
        {
            
            
            return "logs/index";
            
        }catch(Exception ex)
        {
            ex.printStackTrace();
            return "logs/index";
        }
    }
    
    
}
