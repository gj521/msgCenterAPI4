package org.hshk.message.admin;

import org.hshk.message.entity.CpMsgTemplate;
import org.hshk.message.mapper.CpMsgTemplateMapper;
import org.hshk.message.repository.CpMsgTemplateRepository;
import org.hshk.message.utils.PageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created With IntelliJ IDEA
 * User: WangYong
 * Date: 2017/2/28 0028
 * Time: 17:06
 * ProjectName: msgCenterAPI
 * To change this template use File
 */
@Controller
@RequestMapping(value = "/template")
public class AdminTemplateController
{
    @Autowired
    private CpMsgTemplateRepository cpMsgTemplateRepository;
    @Autowired
    private CpMsgTemplateMapper cpMsgTemplateMapper;
    
    private static final int PAGE_SIZE = 15;
    
    
    @RequestMapping(value = "/index")
    public String index(Model model)
    {
        PageUtils pageUtils = new PageUtils();
        Map<String, Object> paramMap = new HashMap<String, Object>();
        int totalSize = cpMsgTemplateMapper.findByParametersAndPage(paramMap).size();
        pageUtils.setTotalSize(totalSize);
        paramMap.put("startPoint", 0);
        paramMap.put("endPoint", PAGE_SIZE);
        List<CpMsgTemplate> templateList = cpMsgTemplateMapper.findByParametersAndPage(paramMap);
        pageUtils.setCurrentPage(1);
        if(totalSize % PAGE_SIZE == 0)
        {
            pageUtils.setPages(totalSize / PAGE_SIZE);
        }else
        {
            pageUtils.setPages(totalSize / PAGE_SIZE + 1);
        }
        pageUtils.setPageSize(PAGE_SIZE);
        model.addAttribute("pageUtils", pageUtils);
        model.addAttribute("templateList", templateList);
        
        //查询出产品类型列表
        List<Map<String, Object>> productTypeList = cpMsgTemplateMapper.findAllProductType();
        model.addAttribute("productTypeList", productTypeList);
        return "template/index";
    }
    
    @RequestMapping(value = "/query")
    public String getByCondition(Model model, String brandId, String productType,
                                 String templateCode, String currentPage)
    {
        try
        {
            PageUtils pageUtils = new PageUtils();
            Map<String, Object> paramMap = new HashMap<String, Object>();
            
            int totalSize = cpMsgTemplateMapper.findByParametersAndPage(paramMap).size();
            pageUtils.setPageSize(PAGE_SIZE);
            
            if(null != brandId && !"".equals(brandId))
            {
                paramMap.put("brandId", brandId);
            }
            
            if(null != productType && !"".equals(productType))
            {
                paramMap.put("productType", productType);
            }
            
            if(null != templateCode && !"".equals(templateCode))
            {
                paramMap.put("templateCode", templateCode);
            }
            
            if(null != currentPage && !"".equals(currentPage))
            {
                paramMap.put("startPoint", (Integer.parseInt(templateCode) - 1 ) * PAGE_SIZE );
                paramMap.put("endPoint", PAGE_SIZE);
                pageUtils.setCurrentPage(Integer.parseInt(currentPage));
            }else
            {
                pageUtils.setCurrentPage(1);
            }
            pageUtils.setTotalSize(totalSize);
            if(totalSize % PAGE_SIZE == 0)
            {
                pageUtils.setPages(totalSize / PAGE_SIZE);
            }else
            {
                pageUtils.setPages(totalSize / PAGE_SIZE + 1);
            }
            List<CpMsgTemplate> templateList = cpMsgTemplateMapper.findByParametersAndPage(paramMap);
            model.addAttribute("templateList", templateList);
            model.addAttribute("pageUtils", pageUtils);
            return "template/index";
        }catch(Exception ex)
        {
            ex.printStackTrace();
            return "template/index";
        }
    }
    
    
    
    
    
    
}