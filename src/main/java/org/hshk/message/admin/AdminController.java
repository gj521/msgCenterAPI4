package org.hshk.message.admin;

import com.mysql.jdbc.util.Base64Decoder;
import org.hshk.message.entity.CpMsgFactory;
import org.hshk.message.service.CpMsgFactoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;
import java.util.List;

/**
 * Created With IntelliJ IDEA
 * User: WangYong
 * Date: 2017/2/27 0027
 * Time: 11:41
 * ProjectName: msgCenterAPI
 * To change this template use File
 */
@Controller
public class AdminController
{
    @Autowired
    private CpMsgFactoryService cpMsgFactoryService;
    
    @GetMapping(value = "/template")
    public String login(Model model)
    {
        try
        {
            System.out.println("--------- login ---------");
            model.addAttribute("username", "WangYong");
            
            return "login";
        }catch(Exception ex)
        {
            ex.printStackTrace();
            return "login";
        }
    }

    /**
     * 2017年2月28日 15:11:10
     * 首页跳转
     * @param model
     * @return
     */
    @RequestMapping(value = "/")
    public String index(Model model)
    {
        try
        {
            List<CpMsgFactory> cpMsgFactoryList = cpMsgFactoryService.getFactories();
            model.addAttribute("allFactoryList", cpMsgFactoryList);

            
            return "index";
        }catch(Exception ex)
        {
            ex.printStackTrace();
            return "index";
        }
    }
}
