package org.hshk.message.admin;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created With IntelliJ IDEA
 * User: WangYong
 * Date: 2017/3/1 0001
 * Time: 14:24
 * ProjectName: msgCenterAPI
 * To change this template use File
 */
@Controller
@RequestMapping(value = "/factories")
public class AdminFactoryController
{
    
    @RequestMapping(value = "/index")
    public String index(Model model)
    {
        
        
        return "factory/index";
    }
    
    
}
