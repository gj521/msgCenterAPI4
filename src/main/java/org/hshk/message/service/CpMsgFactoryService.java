package org.hshk.message.service;


import org.hshk.message.entity.CpMsgFactory;
import org.hshk.message.repository.CpMsgFactoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created With IntelliJ IDEA
 * User: WangYong
 * Date: 2017/2/24 0024
 * Time: 17:06
 * ProjectName: msgCenterAPI
 * To change this template use File
 */
@Service
public class CpMsgFactoryService
{
    @Autowired
    private CpMsgFactoryRepository repository;

    /**
     * 2016年12月1日 14:21:29
     * 使用短信厂家主键编码获取厂家名称
     * @param factoryId
     * @return
     */
    public String getFactoryNameById(int factoryId)
    {
        try
        {
            CpMsgFactory cpMsgFactory = repository.findById(factoryId);
            return cpMsgFactory.getFactoryName();
        }catch(Exception ex)
        {
            ex.printStackTrace();
            return "Null";
        }
    }

    /**
     * 2016年12月27日 10:12:41
     * 查询出所有的短信厂家
     * @return
     */
    public List<CpMsgFactory> getFactories()
    {
        //查询出来的是 短信厂家 类
        List<CpMsgFactory> factoryList = new ArrayList<CpMsgFactory>();
        try
        {
            //返回所有的短信厂家信息
            return repository.findAll();
        }catch(Exception ex)
        {
            //异常出现后返回空集合
            ex.printStackTrace();
            return factoryList;
        }
    }
}