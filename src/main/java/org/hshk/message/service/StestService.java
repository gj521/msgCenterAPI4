package org.hshk.message.service;

import org.hshk.message.entity.CpMsgMember;
import org.hshk.message.repository.CpMsgMemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Administrator on 2017/3/30.
 */
@Service
public class StestService {
    @Autowired
    CpMsgMemberRepository cpMsgMemberRepository;
    public CpMsgMember run(int i ,int j){
        return cpMsgMemberRepository.findByOrgIdAndBrandId(i,j);
    }

}
