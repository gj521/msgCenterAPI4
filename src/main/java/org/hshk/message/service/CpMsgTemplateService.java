package org.hshk.message.service;


import org.hshk.message.entity.CpMsgTemplate;
import org.hshk.message.repository.CpMsgTemplateRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created With IntelliJ IDEA
 * User: WangYong
 * Date: 2017/2/24 0024
 * Time: 09:42
 * ProjectName: msgCenterAPI
 * To change this template use File
 */
@Service
public class CpMsgTemplateService
{
    //日志记录类
    private static final Logger log = LoggerFactory.getLogger(CpMsgTemplateService.class);
    @Autowired
    private CpMsgTemplateRepository repository;

    /**
     * 2017年2月24日 21:49:27
     * 使用品牌编码、模版代码获取对应的模版列表
     * @param uid
     * @param templateCode
     * @return
     */
    public String findByUidAndTemplateCode(int uid, String templateCode)
    {
        try
        {
            //执行查询，并赋值给templateList集合
            List<CpMsgTemplate> templateList = repository.findByBrandIdAndTemplateCode(uid, templateCode);
            //集合不为空时，返回首个对象的模版正文
            if(null != templateList && 0 < templateList.size())
            {
                return templateList.get(0).getMsgContent();
            }else
            {
                return "";
            }
        }catch(Exception ex)
        {
            ex.printStackTrace();
            return "";
        }
    }

    /**
     * 2017年2月27日 11:06:14
     * 使用品牌编码查看该品牌下是否已经存在了模版
     * @param brandId
     * @return
     */
    public List<CpMsgTemplate> findByBrandId(int brandId)
    {
        try
        {
            //执行查询，并赋值给templateList集合
            List<CpMsgTemplate> templateList = repository.findByBrandId(brandId);
            //集合不为空时，返回首个对象的模版正文
            if(null != templateList && 0 < templateList.size())
            {
                //已经存在了模版
                return templateList;
            }else
            {
                //不存在模版
                return null;
            }
        }catch(Exception ex)
        {
            ex.printStackTrace();
            return null;
        }
    }


    /**
     * 2017年2月27日 10:58:31
     * 向某品牌下添加模版，注意不止一条
     * @param cpMsgTemplateList
     * @return
     */
    public boolean saveTemplate(List<CpMsgTemplate> cpMsgTemplateList, int brandId)
    {
        int nowDate = (int)(System.currentTimeMillis() / 1000);
        try
        {
            for(CpMsgTemplate cpMsgTemplate : cpMsgTemplateList)
            {
                CpMsgTemplate goalTemplate = new CpMsgTemplate();
                goalTemplate.setBrandId(brandId);
                goalTemplate.setUpdateDate(nowDate);
                goalTemplate.setCreateDate(nowDate);
                goalTemplate.setMsgContent(cpMsgTemplate.getMsgContent());
                goalTemplate.setProductType(cpMsgTemplate.getProductType());
                goalTemplate.setOrgId(cpMsgTemplate.getOrgId());
                goalTemplate.setTemplateCode(cpMsgTemplate.getTemplateCode());
                repository.save(goalTemplate);
            }
            return true;
        }catch(Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
    }

}
