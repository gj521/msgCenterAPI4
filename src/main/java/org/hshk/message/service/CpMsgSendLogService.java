package org.hshk.message.service;


import org.hshk.message.entity.CpMsgMemberSign;
import org.hshk.message.entity.CpMsgSendLog;
import org.hshk.message.repository.CpMsgMemberSignRepository;
import org.hshk.message.repository.CpMsgSendLogRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created With IntelliJ IDEA
 * User: WangYong
 * Date: 2017/2/24 0024
 * Time: 17:07
 * ProjectName: msgCenterAPI
 * To change this template use File
 */
@Service
public class CpMsgSendLogService
{
    @Autowired
    private CpMsgSendLogRepository repository;
    @Autowired
    private CpMsgMemberSignRepository cpMsgMemberSignRepository;
    //日志记录类
    private static final Logger log = LoggerFactory.getLogger(CpMsgSendLogService.class);
    /**
     * 2017年2月24日 22:26:45
     * 保存日志操作对象
     * @param cpMsgSendLog
     * @return
     */
    public CpMsgSendLog save(CpMsgSendLog cpMsgSendLog)
    {
        return repository.save(cpMsgSendLog);
    }

    /**
     * 2017年2月27日 09:54:54
     * 由于短信欠费而造成的发送失败，切换短信发送平台
     * @param brandId
     * @return
     */
    @Transactional
    public boolean changeMsgPlatForm(int brandId)
    {
        try
        {
            //首先查询出当前正在用的短信平台
            List<CpMsgMemberSign> cpMsgMemberSignNowList = 
                    cpMsgMemberSignRepository.findByBrandIdAndIsUsed(brandId, "Yes");
            //其次查询出当前没有在用的平台
            List<CpMsgMemberSign> cpMsgMemberSignNoList =
                    cpMsgMemberSignRepository.findByBrandIdAndIsUsed(brandId, "No");
            //如果除了当前再用的平台，没有其他的平台帐号集合，就不执行切换
            if(null !=cpMsgMemberSignNoList && 0 < cpMsgMemberSignNoList.size())
            {
                CpMsgMemberSign cpMsgMemberSignNo = cpMsgMemberSignNoList.get(0);
                
                if(null != cpMsgMemberSignNowList && 0 < cpMsgMemberSignNowList.size())
                {
                    CpMsgMemberSign cpMsgMemberSignNow = cpMsgMemberSignNowList.get(0);
                    cpMsgMemberSignNow.setIsUsed("No");
                    cpMsgMemberSignNo.setIsUsed("Yes");
                    cpMsgMemberSignRepository.save(cpMsgMemberSignNo);
                    cpMsgMemberSignRepository.save(cpMsgMemberSignNow);
                    log.info("切换短信平台成功 [ brandId=" + brandId + 
                            ", 原短信平台FactoryId=" + cpMsgMemberSignNow.getFactoryId() + 
                            ", 新短信平台FactoryId=" + cpMsgMemberSignNo.getFactoryId());
                }
            }
            return true;
        }catch(Exception ex)
        {
            ex.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            log.error(ex.getMessage() + " 切换短信平台出错.");
            return false;
        }
    }
}