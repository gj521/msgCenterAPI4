package org.hshk.message.service;

import org.hshk.message.mapper.CpMsgPayruleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by yxl on 2017-03-29.
 */
@Service
public class PayruleService {
    @Autowired
    private CpMsgPayruleMapper mapper;

    public List<Map<String, Object>> getPayrules(Map<String, Object> paramMap){
        return mapper.getPayrules(paramMap);
    }
}
