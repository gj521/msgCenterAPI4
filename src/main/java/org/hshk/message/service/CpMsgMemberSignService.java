package org.hshk.message.service;


import org.hshk.message.entity.CpMsgMemberSign;
import org.hshk.message.repository.CpMsgMemberSignRepository;
import org.hshk.message.storage.PersistenceMemeryContent;
import org.hshk.message.storage.PersistenceMemeryFactory;
import org.hshk.message.utils.GthdMessageImpl;
import org.hshk.message.utils.HywxMessageImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created With IntelliJ IDEA
 * User: WangYong
 * Date: 2017/2/24 0024
 * Time: 17:06
 * ProjectName: msgCenterAPI
 * To change this template use File
 */
@Service
public class CpMsgMemberSignService
{
    @Autowired
    private PersistenceMemeryContent persistenceMemeryContent;
    @Autowired
    private CpMsgMemberSignRepository cpMsgMemberSignRepository;
    @Autowired
    private PersistenceMemeryFactory persistenceMemeryFactory;
    @Autowired
    private HywxMessageImpl hywxMessage;
    @Autowired
    private GthdMessageImpl gthdMessage;
    //日志记录类
    private static final Logger log = LoggerFactory.getLogger(CpMsgMemberSignService.class);


    /**
     * 2017年2月24日 22:58:51
     * 发送短信 - 路由器
     * @param brandId
     * @param mobile
     * @param templateCode
     * @param parameters
     * @return
     */
    public boolean sendMessage(int brandId, String mobile,
                               String templateCode, String[] parameters)
    {
        try
        {
            String content = concatAndReplaceStr(
                    persistenceMemeryContent.getMsgContent(brandId+ "%" + templateCode), parameters)
                    .replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\"", "");

            List<CpMsgMemberSign> cpMsgMemberSignList = cpMsgMemberSignRepository.findByBrandIdAndIsUsed(brandId, "yes");
            if(null != cpMsgMemberSignList && 0 < cpMsgMemberSignList.size())
            {
                //获取短信厂家ID
                int factoryId = cpMsgMemberSignList.get(0).getFactoryId();
                //获取内存保存的短信厂家对象
                Map<String, Object> resultMap = persistenceMemeryFactory.getFactoryInfo(brandId + "", factoryId);
                //选择要发送短信的厂家
                String factoryName = resultMap.get("_factory_name_").toString().trim();
                switch (factoryName)
                {
                    case "互亿无线" :
                        hywxMessage.send(brandId, content, resultMap.get("_factory_username_").toString(),
                                resultMap.get("_factory_password_").toString(), mobile, "");
                        break;
                    case "光通互动" :
                        gthdMessage.send(brandId, resultMap.get("_factory_sign_").toString() + content,
                                resultMap.get("_factory_username_").toString(),
                                resultMap.get("_factory_password_").toString(), mobile, resultMap.get("_factory_extno_").toString());
                        break;
                    default :
                        //不设置默认值
                        break;
                }
            }
            return true;
        }catch(Exception ex)
        {
            ex.printStackTrace();
            return false;
        }
    }

    /**
     * 2017年2月27日 10:28:01
     * 查询余额
     * @param brandId
     * @return
     */
    public String overageMessage(int brandId)
    {
        //返回的余额参数等接口
        int balanceCount = 0;
        try
        {
            List<CpMsgMemberSign> cpMsgMemberSignList = cpMsgMemberSignRepository.findByBrandIdAndIsUsed(brandId, "Yes");
            if(null != cpMsgMemberSignList && 0 < cpMsgMemberSignList.size())
            {
                /**
                 * 获取当前短信厂家的用户名和密码等信息
                 */
                int factoryId = cpMsgMemberSignList.get(0).getFactoryId();
                //找到商户对应短信厂家的用户名和密码
                Map<String, Object> resultMap = persistenceMemeryFactory.getFactoryInfo(brandId + "", factoryId);
                switch (resultMap.get("_factory_name_").toString().trim())
                {
                    case "互亿无线" :
                        //调用互亿无线的短信接口
                        balanceCount = hywxMessage.overage(brandId, resultMap.get("_factory_username_").toString().trim(),
                                resultMap.get("_factory_password_").toString());
                        break;
                    case "光通互动" :
                        //调用光通互动的短信接口
                        balanceCount = gthdMessage.overage(brandId, resultMap.get("_factory_username_").toString().trim(),
                                resultMap.get("_factory_password_").toString().trim());
                        break;
                    default :

                        break;
                }
            }
        }catch(Exception ex)
        {
            ex.printStackTrace();
            return ex.getMessage();
        }
        return String.valueOf(balanceCount);
    }



    /**
     * 2017年2月24日 22:59:26
     * 变量替换方法，将字符串中的【变量】替换成指定的参数
     * @param content
     * @param params
     * @return
     */
    public static String concatAndReplaceStr(String content, String[] params)
    {
        StringBuffer result = new StringBuffer();
        int num = 0;
        for(String temp : content.split("【变量】"))
        {
            result.append(temp);
            if(params.length > num)
            {
                result.append(params[num]);
            }
            num ++;
        }
        return result.toString().replaceAll("Br", "\n");
    }
}