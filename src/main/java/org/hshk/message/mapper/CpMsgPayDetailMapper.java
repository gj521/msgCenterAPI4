package org.hshk.message.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.hshk.message.entity.CpMsgMember;
import org.hshk.message.entity.CpMsgPayDetail;

import java.util.List;
import java.util.Map;
import java.util.Objects;

@Mapper
public interface CpMsgPayDetailMapper {
    //根据支付状态,去查询支付详情,并分页显示,下面的id1和id2,是每页查询的起始条数和终止条数(转jpa完成)
    @Select("SELECT id," +
            "uid," +
            "brand_id brandId," +
            "payrule_id payruleId," +
            "pay_count payCount," +
            "pay_amount payAmount," +
            "pay_type payType," +
            "order_no orderNo,"+
            "status status,"+
            "create_date createDate," +
            "update_date updateDate,factory_id factoryId FROM msg_pay_detail " +
            "WHERE status = #{status}  order by id desc limit #{id1},#{id2} ")
       List<CpMsgPayDetail> findByid1Andid2 (@Param("id1") int id1, @Param("id2")int id2,@Param("status")String status);
    @Select(" <script> SELECT id," +
            "uid ," +
            "brand_id brandId," +
            "payrule_id payruleId," +
            "pay_count payCount," +
            "pay_amount payAmount," +
            "pay_type payType," +
            "order_no orderNo,"+
            "status status,"+
            "create_date createDate," +
            "update_date updateDate,factory_id factoryId FROM msg_pay_detail "+
            " WHERE " +
            "1+1"+
            " and status = #{pa.finished}"+
            " <if test=\"#{pa.orgId} != null\">and uid = #{pa.orgId} </if> "+
            " <if test=\"#{pa.brandId} != null \">and brand_id = #{pa.brandId} </if> "+
            " <if test=\"#{pa.id1} != null \">order by id desc limit #{pa.id1},#{pa.id2} </if> </script>")
    List<CpMsgPayDetail> findByid1Andid3 (@Param("pa") Map<String, Object> pa);


    //统计充值记录的数据量(转jpa完成)
    @Select("select * from  msg_pay_detail where status = #{status}")
    List<CpMsgPayDetail> findCount (@Param("status") String  status);

    //更改状态(转jpa完成)
    @Update("update msg_pay_detail set " +
            "status = #{status} where id = #{id}")
    void upDateByStatus(@Param("id") int id, @Param("status") String status);

//主要为了查询他的大小(转jpa完成)
    @Select("SELECT id," +
            "uid," +
            "brand_id brandId," +
            "payrule_id payruleId," +
            "pay_count payCount," +
            "pay_amount payAmount," +
            "pay_type payType," +
            "order_no orderNo,"+
            "status status,"+
            "create_date createDate," +
            "update_date updateDate,factory_id factoryId FROM msg_pay_detail " +
            "WHERE status = #{status}  and uid = #{uid} and brand_id = #{brandId}")
    List<CpMsgPayDetail> findByUidAndBrandIdAndStatus (@Param("uid") int uid, @Param("brandId")int brandId,
                                                       @Param("status") String status);


    //根据条件查询厂商的东西(转jpa完成)
    @Select("SELECT id," +
            "uid," +
            "brand_id brandId," +
            "payrule_id payruleId," +
            "pay_count payCount," +
            "pay_amount payAmount," +
            "pay_type payType," +
            "order_no orderNo,"+
            "status status,"+
            "create_date createDate," +
            "update_date updateDate,factory_id factoryId FROM msg_pay_detail " +
            "WHERE status = #{status}  and uid = #{uid} and brand_id = #{brandId} and factory_id = #{factoryId}")
    List<CpMsgPayDetail> findByUidAndBrandIdAndStatusAndFactoryId (@Param("uid") int uid, @Param("brandId")int brandId,
                                                       @Param("status") String status,@Param("factoryId") int factoryId);
    
    
}
