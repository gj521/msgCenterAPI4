package org.hshk.message.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

@Mapper
public interface CpMsgPayruleMapper
{
    @Select("<script>SELECT " +
            "`id` id, " +
            "`payrule_name` payruleName, " +
            "`begin_expiry_date` beginExpiryDate, " +
            "`end_expiry_date` endExpiryDate, " +
            "`create_date` createDate, " +
            "`update_date` updateDate " +
            "FROM " +
            "msg_payrule  " +
            "ORDER BY id " +
            "<if test=\"param.startPoint != null\"> LIMIT #{param.startPoint}, #{param.endPoint} </if> " +
            "</script>")
    List<Map<String, Object>> getPayrules(@Param("param") Map<String, Object> paramMap);
}
