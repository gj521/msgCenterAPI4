package org.hshk.message.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.hshk.message.entity.CpMsgMember;

import java.util.List;

@Mapper
public interface CpMsgMemberMapper {
    //短信商户的分页查询(转jpa完成)
    @Select("SELECT id," +
            "uid orgId," +
            "brand_id brandId," +
            "payrule_id payruleId," +
            "total_count totalCount," +
            "balance_count balanceCount," +
            "alarm_mobiles alarmMobiles," +
            "create_date createDate," +
            "update_date updateDate FROM msg_member " +
            "order by id limit #{id1},#{id2}")
    List<CpMsgMember> findById1AndId2(@Param("id1") int id1, @Param("id2") int id2);

    //统计短信商家的数据量(转jpa完成)
    @Select("select count(*) from  msg_member")
    int findCount();

    //短信商家的充值规则(转jpa完成)
    @Update("update msg_member set " +
            "payrule_id = #{payruleId} ,alarm_mobiles = #{alarmMobiles}" +
            "where uid = #{uid} and brand_id = #{brandId}")
    void upDateByPayruleId(@Param("uid") int uid, @Param("brandId") int brandId, @Param("payruleId") int payruleId, @Param("alarmMobiles") String alarmMobiles);

    //条数更改和修改(转jpa完成)
    @Update("update msg_member set " +
            "balance_count = #{balanceCount} " +
            "where uid = #{uid} and brand_id = #{brandId}")
    void upDateByBalanceCount(@Param("uid") int uid, @Param("brandId") int brandId, @Param("balanceCount") int balanceCount);

    //查询商家信息(转jpa完成)
    @Select("SELECT id," +
            "uid orgId," +
            "brand_id brandId," +
            "payrule_id payruleId," +
            "total_count totalCount," +
            "balance_count balanceCount," +
            "alarm_mobiles alarmMobiles," +
            "create_date createDate," +
            "update_date updateDate FROM msg_member " +
            "WHERE uid = #{orgId} and brand_id = #{brandId}")
    CpMsgMember findByUidANDBrandId(@Param("orgId") int orgId, @Param("brandId") int brandId);


    //条数更改和修改(转jpa完成)
    @Update("update msg_member set " +
            "balance_count = #{balanceCount} , total_count = #{totalCount} , update_date = #{updateDate} WHERE id = #{id}")
    void upDateByBalanceCountAndTotalCountAndUpdateDate(@Param("id") int id, @Param("balanceCount") int balanceCount,
                                                        @Param("totalCount") int totalCount,
                                                        @Param("updateDate") int updateDate);
}
