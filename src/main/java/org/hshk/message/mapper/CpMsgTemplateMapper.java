package org.hshk.message.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.hshk.message.entity.CpMsgTemplate;

import java.util.List;
import java.util.Map;

/**
 * Created With IntelliJ IDEA
 * User: WangYong
 * Date: 2017/2/24 0024
 * Time: 17:25
 * ProjectName: msgCenterAPI
 * To change this template use File
 */
@Mapper
public interface CpMsgTemplateMapper
{
    @Select("<script>" +
            "SELECT " +
            " id , " +
            " uid orgId " +
            " brand_id brandId, " +
            " product_type productType, " +
            " template_code templateCode, " +
            " msg_content msgContent, " +
            " create_date createDate, " +
            " update_date updateDate " +
            "FROM msg_template  " +
            "WHERE  " +
            " 1 = 1  " +
            " <if test=\"param.brandId != null\">AND brand_id = #{param.brandId} </if> " +
            " <if test=\"param.productType != null\">AND product_type = #{param.productType} </if> " +
            " <if test=\"param.templateCode != null\"> AND template_code = #{param.templateCode} </if> " +
            "ORDER BY id " +
            " <if test=\"param.endPoint != null\"> LIMIT #{param.startPoint}, #{param.endPoint} </if>" +
            "</script>")
    List<CpMsgTemplate> findByParametersAndPage(@Param("param") Map<String, Object> param);
    
    //DISTINCT 是去重的关键字
    @Select("SELECT DISTINCT product_type FROM msg_template ")
    List<Map<String, Object>> findAllProductType();
    
    
}
