package org.hshk.message.mapper;


import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.hshk.message.entity.CpMsgMemberSign;

import java.util.List;

/**
 * Created With IntelliJ IDEA
 * User: WangYong
 * Date: 2017/2/24 0024
 * Time: 17:25
 * ProjectName: msgCenterAPI
 * To change this template use File
 */
@Mapper

//商户签名
public interface CpMsgMemberSignMapper
{
    @Select("UPDATE msg_member_sign SET sms_balance = #{smsBalance} WHERE id = #{id}")
    void upDateSmsBalance (@Param("id") int id, @Param("smsBalance") int smsBalance);

    //"SELECT DISTINCT product_type FROM msg_template "
    @Select("SELECT * FROM msg_member_sign WHERE is_used = #{isUsed}" )
    List<CpMsgMemberSign> findByIsUsed(@Param("isUsed") String isUsed);
    
    
}
