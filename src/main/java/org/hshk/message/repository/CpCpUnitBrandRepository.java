package org.hshk.message.repository;

import org.hshk.message.entity.CpMsgPayrule;

import org.hshk.message.entity.CpUnitBrand;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created With IntelliJ IDEA
 * User: WangYong
 * Date: 2017/2/24 0024
 * Time: 17:25
 * ProjectName: msgCenterAPI
 * To change this template use File
 */

//只继承了接口，没有重写任何方法

public interface CpCpUnitBrandRepository extends JpaRepository<CpUnitBrand, Integer> {
}
