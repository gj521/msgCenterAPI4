package org.hshk.message.repository;

import org.hshk.message.entity.CpMsgPayruleDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by yxl on 2017-03-29.
 */


//这个接口里面的方法，一个是根据支付规则的id删除什么
//另一个方法是根据支付规则的id，去获取对应支付规则的详细内容

@Repository
public interface CpMsgPayruleDetailRepository extends JpaRepository<CpMsgPayruleDetail, Integer> {
    @Query(value = "delete from  msg_payrule_detail where payrule_id = :payruleId", nativeQuery = true)
    @Modifying
    @Transactional
    public void deleteByPayruleId(@Param("payruleId") int payruleId);

    List<CpMsgPayruleDetail> getByPayruleId(int payruleId);
}
