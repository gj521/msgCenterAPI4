package org.hshk.message.repository;

import org.hshk.message.entity.CpMsgFactory;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created With IntelliJ IDEA
 * User: WangYong
 * Date: 2017/2/24 0024
 * Time: 17:25
 * ProjectName: msgCenterAPI
 * To change this template use File
 */

//使用主键，查询出对应的短信厂家

public interface CpMsgFactoryRepository extends JpaRepository<CpMsgFactory, Integer> {

    /**
     * 2017年2月24日 17:42:15
     * 使用主键查询出对应的短信厂家
     *
     * @param id
     * @return
     */
    CpMsgFactory findById(int id);

    /**
     * mapper转jpa
     * 统计短信商家的数量(用默认的count方法就好)
     */
    long count();

}
