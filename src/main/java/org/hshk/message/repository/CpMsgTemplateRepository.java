package org.hshk.message.repository;

import org.hshk.message.entity.CpMsgTemplate;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created With IntelliJ IDEA
 * User: WangYong
 * Date: 2017/2/24 0024
 * Time: 17:25
 * ProjectName: msgCenterAPI
 * To change this template use File
 */

//使用品牌id，去查询对应的短信模版

public interface CpMsgTemplateRepository extends JpaRepository<CpMsgTemplate, Integer>
{
    /**
     * 2017年2月24日 21:49:27
     * 使用品牌编码、模版代码获取对应的模版列表
     * @param uid
     * @param templateCode
     * @return
     */
    List<CpMsgTemplate> findByBrandIdAndTemplateCode(int uid, String templateCode);

    /**
     * 2017年2月27日 11:05:15
     * 使用品牌编码查看是否该品牌下已经存在了模版
     * @param brandId
     * @return
     */
    List<CpMsgTemplate> findByBrandId(int brandId);


    List<CpMsgTemplate> findByProductType(String productType);
}
