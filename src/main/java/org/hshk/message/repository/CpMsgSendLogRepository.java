package org.hshk.message.repository;

import org.hshk.message.entity.CpMsgSendLog;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created With IntelliJ IDEA
 * User: WangYong
 * Date: 2017/2/24 0024
 * Time: 17:25
 * ProjectName: msgCenterAPI
 * To change this template use File
 */

//集成了jpa的接口，但并没有实现接口中的方法

public interface CpMsgSendLogRepository extends JpaRepository<CpMsgSendLog, Integer>
{
    
    
    
}
