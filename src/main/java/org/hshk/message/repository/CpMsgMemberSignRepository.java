package org.hshk.message.repository;

import org.hshk.message.entity.CpMsgMemberSign;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created With IntelliJ IDEA
 * User: WangYong
 * Date: 2017/2/24 0024
 * Time: 17:25
 * ProjectName: msgCenterAPI
 * To change this template use File
 *
 */

//商户签名
//查询短信厂家。和使用厂家的id去查询单条记录

public interface CpMsgMemberSignRepository extends JpaRepository<CpMsgMemberSign, Integer> {
    /**
     * 2017年2月24日 22:55:00
     * 使用品牌编码和是否正在使用查询出短信厂家签名列表
     *
     * @param brandId
     * @param isUsed
     * @return
     */
    List<CpMsgMemberSign> findByBrandIdAndIsUsed(int brandId, String isUsed);

    /**
     * 2017年3月7日 11:06:40
     * 查询出当前所有商户正在用的短信厂家列表
     *
     * @param isUsed
     * @return
     */
    List<CpMsgMemberSign> findByIsUsed(String isUsed);

    /**
     * 2017年3月7日 11:06:40
     * 根据短信商家id和品牌id查询单条记录
     *
     * @param orgId
     * @param brandId
     * @return
     */
    //根据商家id，品牌id，是否正在使用，去查询，使用的是哪一家 短信厂家
    CpMsgMemberSign findByBrandIdAndOrgIdAndIsUsed(int brandId, int orgId, String isUsed);

    List<CpMsgMemberSign> findByBrandIdAndOrgId(int brandId, int orgId);

    /**
     * 2017年3月7日 11:06:40
     * 根据短信厂商id和品牌id查询单条记录
     *
     * @param brandId
     * @param factoryId
     * @return
     */
    CpMsgMemberSign findByBrandIdAndFactoryId(int brandId, int factoryId);


}
