package org.hshk.message.repository;

import org.hshk.message.entity.CpMsgPayrule;
import org.hshk.message.entity.CpMsgPayruleDetail;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created With IntelliJ IDEA
 * User: gc
 * Date: 2017/3/29
 * Time: 13:19
 */

//跟据id，查询充值规则的的条数，和所有信息

public interface MsgPayruleDetailRepository extends JpaRepository<CpMsgPayruleDetail, Integer>
{
    /**
     * 2017年2月24日 17:42:15
     * 根据id查询充值规则条数所有信息
     * @param id
     * @return
     */
    CpMsgPayruleDetail findById(int id);


    
    
    
}
