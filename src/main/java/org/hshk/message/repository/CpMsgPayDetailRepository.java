package org.hshk.message.repository;


import org.hshk.message.entity.CpMsgPayDetail;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;

/**
 * Created With IntelliJ IDEA
 * User: WangYong
 * Date: 2017/2/24 0024
 * Time: 17:25
 * ProjectName: msgCenterAPI
 * To change this template use File
 */

//使用短信商的uid查询所有信息

public interface CpMsgPayDetailRepository extends JpaRepository<CpMsgPayDetail, Integer> {
    /**
     * 2017年2月24日 17:42:15
     * 根据uid短信商户查询所有
     *
     * @param cpMsgPayDetail
     * @return
     */
    //将支付详情对象保存到数据库
    CpMsgPayDetail save(CpMsgPayDetail cpMsgPayDetail);

    //根据支付状态,查询支付详情
    List<CpMsgPayDetail> findByStatus(String status);

    /**
     * 根据 支付状态 查询 支付详情 并降序分页显示(这个Pageable分页属性,里面自带升序降序的属性了)
     * mapper转jpa
     */
    //@Transactional()
    Page<CpMsgPayDetail> findByStatus(String status, Pageable pageable);

    /**
     * 统计充值记录的数据量
     * mapper转jpa
     */
    long countByStatus(String status);

    /**
     * 更改充值状态
     * mapper转jpa
     */
    @Transactional
    @Modifying
    @Query(value = "UPDATE CpMsgPayDetail set status = :status where id = :id")
    int updateById(@Param("id") int id,
                   @Param("status") String status);

    /**
     * 根据 商家Id,品牌Id,支付状态查询 支付详情
     * mapper转jpa
     */
    List<CpMsgPayDetail> findByUidAndBrandIdAndStatus(int uid, int brandId, String status);

    /**
     * 根据 商家Id,品牌Id,支付状态,短信厂商id 查询 支付详情
     * mapper转jpa
     */
    List<CpMsgPayDetail> findByUidAndBrandIdAndStatusAndFactoryId(int uid, int brandId, String status, int factoryId);
}
