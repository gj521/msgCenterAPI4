package org.hshk.message.repository;


import org.hshk.message.entity.CpMsgMember;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created With IntelliJ IDEA
 * User: WangYong
 * Date: 2017/2/24 0024
 * Time: 17:25
 * ProjectName: msgCenterAPI
 * To change this template use File
 */

//商户信息查询


@Transactional
public interface CpMsgMemberRepository extends JpaRepository<CpMsgMember, Integer> {
    /**
     * 2017年2月24日 17:42:15
     * 根据uid短信商户查询所有
     *
     * @param orgId
     * @return
     */
    List<CpMsgMember> findByOrgId(int orgId);

    /**
     * 2017年2月24日 17:42:15
     * 根据uid和brandId获取 充值规则id
     *
     * @param orgId
     * @return
     */
    CpMsgMember findByOrgIdAndBrandId(int orgId, int brandId);

    @Transactional
    CpMsgMember save(CpMsgMember cpMsgMember);


    /**
     * 2017年2月24日 17:42:15
     * 根据brandId获取 所有
     *
     * @param brandId
     * @return List<CpMsgMember>
     */
    List<CpMsgMember> findByBrandId(int brandId);

    /**
     * 2017年2月24日 17:42:15
     * 根据payruleId获取 所有
     *
     * @param payruleId
     * @return List<CpMsgMember>
     */
    List<CpMsgMember> findByPayruleId(int payruleId);


    /**
     * 分页展示用户信息
     * mapper转jpa
     */

    Page<CpMsgMember> findAll(Pageable pageable);

    /**
     * 统计短信商家的数据量
     * mapper转jpa
     */
    long count();

    /**
     * 修改短信商家的充值规则
     * mapper转jpa
     */
    @Transactional
    @Modifying
    @Query(value = "UPDATE CpMsgMember set payruleId = :payruleId where orgId = :orgId AND brandId = :brandId AND alarmMobiles = :alarmMobiles")
    int updateByorgIdAndbrandIdAndalarmMobiles(@Param("payruleId") int payruleId,
                                               @Param("orgId") int orgId,
                                               @Param("brandId") int brandId,
                                               @Param("alarmMobiles") String alarmMobiles);

    /**
     * 条数更改和修改(短信剩余条数)
     * mapper转jpa
     */
    @Transactional
    @Modifying
    @Query(value = "UPDATE CpMsgMember  set balanceCount = :balanceCount where orgId = :orgId AND brandId = :brandId")
    int updateByorgIdAndbrandId(@Param("balanceCount") int balanceCount,
                                @Param("orgId") int orgId,
                                @Param("brandId") int brandId);

    /**
     * 根据商家id,品牌id,查询商家信息
     * mapper转jpa
     */
    List<CpMsgMember> findByBrandIdAndOrgId(int brandId, int orgId);

    /**
     * 根据id,修改商家信息
     * mapper转jpa
     */
    @Transactional
    @Modifying
    @Query(value = "UPDATE CpMsgMember set balanceCount = :balanceCount ,totalCount= :totalCount, updateDate= :updateDate where id = :id")
    int upDateByBalanceCountAndTotalCountAndUpdateDate(@Param("balanceCount") int balanceCount,
                                                       @Param("totalCount") int totalCount,
                                                       @Param("updateDate") int updateDate,
                                                       @Param("id") int id);

    /**
     * 尝试(根据id降序,根据uid升序)
     */
    //List<CpMsgMember> findByOrderByIdDescOrgIdEsc();
}
