package org.hshk.message.inDto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hshk.message.entity.CpMsgPayruleDetail;

import java.util.Date;
import java.util.List;

/**
 * Created by yxl on 2017-03-29.
 */
@ApiModel(value = "InDtoPayrule")
public class InDtoPayrule {
    @ApiModelProperty(name = "id", value = "充值规则id")
    private int id;//  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键，唯一标识 规则表',

    @ApiModelProperty(name = "payruleName", value = "充值规则名称")
    private String payruleName;//`payrule_name` varchar(100) NOT NULL DEFAULT '' COMMENT '充值规则名称',

    @ApiModelProperty(name = "beginExpiryDate", value = "开始有效期")
    private int beginExpiryDate;//  `begin_expiry_date` date NOT NULL COMMENT '开始有效期',

    @ApiModelProperty(name = "endExpiryDate", value = "结束有效期")
    private int endExpiryDate;//`end_expiry_date` date NOT NULL COMMENT '结束有效期',

    @ApiModelProperty(name = "createDate", value = "创建时间")
    private int createDate;//`create_date` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',

    @ApiModelProperty(name = "updateDate", value = "最后修改时间")
    private int updateDate;//  `update_date` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '最后修改时间',

    @ApiModelProperty(name = "detailList", value = "充值规则明细")
    private List<CpMsgPayruleDetail> detailList;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPayruleName() {
        return payruleName;
    }

    public void setPayruleName(String payruleName) {
        this.payruleName = payruleName;
    }

    public int getBeginExpiryDate() {
        return beginExpiryDate;
    }

    public void setBeginExpiryDate(int beginExpiryDate) {
        this.beginExpiryDate = beginExpiryDate;
    }

    public int getEndExpiryDate() {
        return endExpiryDate;
    }

    public void setEndExpiryDate(int endExpiryDate) {
        this.endExpiryDate = endExpiryDate;
    }

    public int getCreateDate() {
        return createDate;
    }

    public void setCreateDate(int createDate) {
        this.createDate = createDate;
    }

    public int getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(int updateDate) {
        this.updateDate = updateDate;
    }

    public List<CpMsgPayruleDetail> getDetailList() {
        return detailList;
    }

    public void setDetailList(List<CpMsgPayruleDetail> detailList) {
        this.detailList = detailList;
    }

    @Override
    public String toString() {
        return "InDtoPayrule{" +
                "id=" + id +
                ", payruleName='" + payruleName + '\'' +
                ", beginExpiryDate=" + beginExpiryDate +
                ", endExpiryDate=" + endExpiryDate +
                ", createDate=" + createDate +
                ", updateDate=" + updateDate +
                ", detailList=" + detailList +
                '}';
    }
}
