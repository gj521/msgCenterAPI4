package org.hshk.message.inDto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Created by Administrator on 2017/3/30.
 */
@ApiModel(value = "InRechargeHistory")
public class InRechargeHistory {
    @ApiModelProperty(name = "page", value = "当前页码")
    private int page;

    @ApiModelProperty(name = "pageSize", value = "每页多少行")
    private int pageSize;

    @ApiModelProperty(name = "orgId", value = "商品id")
    private int orgId;

    @ApiModelProperty(name = "brandId", value = "品牌id")
    private int brandId;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getOrgId() {
        return orgId;
    }

    public void setOrgId(int orgId) {
        this.orgId = orgId;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    @Override
    public String toString() {
        return "InRechargeHistory{" +
                "page=" + page +
                ", pageSize=" + pageSize +
                ", orgId=" + orgId +
                ", brandId=" + brandId +
                '}';
    }
}
