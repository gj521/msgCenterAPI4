package org.hshk;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Created With IntelliJ IDEA
 * User: WangYong
 * Date: 2017/2/24 0024
 * Time: 09:26
 * ProjectName: msgCenterAPI
 * To change this template use File
 */
@ComponentScan("org.hshk.message")
@EntityScan("org.hshk.message.entity")
@EnableScheduling
@SpringBootApplication
@Configuration
@EnableSwagger2             //启动swagger注解
public class ApplicationBootstrap {
    public static void main(String[] args) {
        try {
            new SpringApplicationBuilder(ApplicationBootstrap.class).web(true).run(args);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("org.hshk.message.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("短信中心（RestFull架构）APIs")
                .description("短信中心的服务接口 API")
                .termsOfServiceUrl("http://swagger.hocyun.cn/")
                .contact("好食好客")
                .version("1.0")
                .build();
    }
}
