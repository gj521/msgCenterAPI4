<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/2/27 0027
  Time: 16:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <title>短信平台</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/plugins/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/global.css" media="all">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/plugins/font-awesome/css/font-awesome.min.css">
</head>
<body>
    <div class="layui-layout layui-layout-admin" style="border-bottom: solid 5px #1aa094;">
        <div class="layui-header header header-demo">
            <div class="layui-main">
                <div class="admin-login-box">
                    <a class="logo" style="left: 0;" href="http://beginner.zhengjinfan.cn/demo/beginner_admin/">
                        <span style="font-size: 22px;">MessageService</span>
                    </a>
                    <div class="admin-side-toggle">
                        <button class="layui-btn">隐藏左侧菜单</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="layui-side layui-bg-black" id="admin-side">
            <div class="layui-side-scroll" id="admin-navbar-side" lay-filter="side">
            </div>
        </div>
        <div class="layui-body" style="bottom: 0;border-left: solid 2px #1AA094;" id="admin-body">
            <div class="layui-tab admin-nav-card layui-tab-brief" lay-filter="admin-tab">
                <ul class="layui-tab-title">
                    <li class="layui-this">
                        <i class="fa fa-dashboard" aria-hidden="true"></i>
                        <cite>控制面板</cite>
                    </li>
                </ul>
                <div class="layui-tab-content" style="min-height: 150px; padding: 5px 0 0 0;">
                    <div class="layui-tab-item layui-show">
                        <div class="admin-main">
                            <blockquote class="layui-elem-quote">
                                <p>1.短信发送接口</p>
                                1.1请求地址：http://www.hocyun.com:10080/sms_platform/message/send
                                1.2参数说明：<br/>
                                <table class="layui-table" lay-skin="line">
                                    <colgroup>
                                        <col width="150">
                                        <col width="150">
                                        <col>
                                    </colgroup>
                                    <thead>
                                    <tr>
                                        <th>参数名称</th>
                                        <th>参数含义</th>
                                        <th>参数说明</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>brandId</td>
                                        <td>品牌编码</td>
                                        <td>品牌编码，为字符串格式。</td>
                                    </tr>
                                    <tr>
                                        <td>mobile</td>
                                        <td>被叫号码</td>
                                        <td>短信发送的目的号码，为字符串格式。</td>
                                    </tr>
                                    <tr>
                                        <td>templateCode</td>
                                        <td>模版编码</td>
                                        <td>短信发送模版编码，为字符串格式。</td>
                                    </tr>
                                    <tr>
                                        <td>parameters</td>
                                        <td>模版参数</td>
                                        <td>短信发送模版所需要的参数，用于替换短信中的变量字符；为数组格式。</td>
                                    </tr>
                                    </tbody>
                                </table>
                                1.3接口调用例子
                                接口地址：
                                http://www.hocyun.com:10080/message/send
                                GET请求时，传递参数：
                                ?brandId=700001&mobile=138xxxxxxxx&templateCode=HMD01&parameters=1,2,3,4,5
                                POST请求时，传递参数：
                                将传递的参数封装在Request会话的上下文中即可。
                            </blockquote>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="layui-footer footer footer-demo" id="admin-footer">
            <div class="layui-main">
                <p>2017 &copy;
                    短信平台，已集成短信厂家：
                    <c:forEach var="factory" items="${allFactoryList}">
                        [&nbsp;${factory.factoryName} &nbsp;]&nbsp;
                    </c:forEach>
                </p>
            </div>
        </div>
        <div class="site-tree-mobile layui-hide">
            <i class="layui-icon">&#xe602;</i>
        </div>
        <div class="site-mobile-shade"></div>
    
        <script type="text/javascript" src="${pageContext.request.contextPath}/statics/plugins/layui/layui.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/statics/datas/nav.js"></script>
        <script src="${pageContext.request.contextPath}/statics/js/index.js"></script>
        <script>
            layui.use('layer', function() {
                var $ = layui.jquery,
                    layer = layui.layer;
    
                $('#video1').on('click', function() {
                    layer.open({
                        title: 'YouTube',
                        maxmin: true,
                        type: 2,
                        content: 'video.html',
                        area: ['800px', '500px']
                    });
                });
            });
        </script>
    </div>
</body>
</html>