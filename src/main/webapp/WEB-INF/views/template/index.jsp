<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/2/27 0027
  Time: 16:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <title>短信平台 - 模版设置</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/plugins/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/global.css" media="all">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/statics/css/table.css" />
</head>
<body>
    <div class="admin-main">
        <blockquote class="layui-elem-quote">
            <!-- 品牌编码 -->
            <form class="layui-form">
                <div class="layui-form-item">
                    <label class="layui-form-label">品牌编码</label>
                    <div class="layui-input-inline">
                        <input type="text" id="conditionBrandId" name="conditionBrandId" required  lay-verify="required" 
                               placeholder="请输入品牌编码" autocomplete="off" class="layui-input">
                    </div>
                    <!-- 产品类型 -->
                    <label class="layui-form-label">产品类型</label>
                    <div class="layui-input-inline">
                        <select id="conditionProductType" name="conditionProductType" lay-verify="" lay-search>
                            <c:forEach var="product" items="${productTypeList}">
                                <option value="">选择产品类型</option>
                                <option value="${product.product_type}">${product.product_type}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <!-- 模版编码 -->
                    <label class="layui-form-label">模版编码</label>
                    <div class="layui-input-inline">
                        <input type="text" id="conditionTemplateCode" name="conditionTemplateCode"  lay-verify="required" 
                               placeholder="请输入模版编码" autocomplete="off" class="layui-input">
                    </div>
                    <!-- 查询 -->
                    <input type="reset" class="layui-btn layui-btn-primary" value="重置条件"/>
                    <input type="button" id="conditionQueryBtn" class="layui-btn" value="执行查询"/>
                </div>
            </form>
        </blockquote>
        <fieldset class="layui-elem-field">
            <legend>短信模版列表</legend>
            <div class="layui-field-box">

                <div id="templateTableDiv" style="border: 1px solid #DDDDDD;"></div>
                
                <table class="site-table table-hover">
                    <thead>
                        <tr>
                            <th><input type="checkbox" id="selected-all"></th>
                            <th>ID</th>
                            <th>品牌编码</th>
                            <th>产品类型</th>
                            <th>模版编码</th>
                            <th>模版正文</th>
                            <th>创建时间</th>
                            <th>修改时间</th>
                            <th>操作</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="template" items="${templateList}">
                            <tr>
                                <td><input type="checkbox"></td>
                                <td>${template.id}</td>
                                <td>
                                    ${template.brandId}
                                </td>
                                <td>${template.productType}</td>
                                <td>${template.templateCode}</td>
                                <td>${template.msgContent}</td>
                                <td>${template.createDate}</td>
                                <td>${template.updateDate}</td>
                                <td>
                                    <a href="/manage/article_edit_1" class="layui-btn layui-btn-mini">编辑</a>
                                    <a href="javascript:;" data-id="1" data-opt="del" class="layui-btn layui-btn-danger layui-btn-mini">删除</a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </fieldset>
    </div>


    <script type="text/javascript" src="${pageContext.request.contextPath}/statics/plugins/layui/layui.js"></script>
    <script>
        layui.use(['icheck', 'laypage','layer'], function() {
            var $ = layui.jquery,
                laypage = layui.laypage,
                layer = parent.layer === undefined ? layui.layer : parent.layer;
            $('input').iCheck({
                checkboxClass: 'icheckbox_flat-green'
            });

            $('.site-table tbody tr').on('click', function(event) {
                var $this = $(this);
                var $input = $this.children('td').eq(0).find('input');
                $input.on('ifChecked', function(e) {
                    $this.css('background-color', '#EEEEEE');
                });
                $input.on('ifUnchecked', function(e) {
                    $this.removeAttr('style');
                });
                $input.iCheck('toggle');
            }).find('input').each(function() {
                var $this = $(this);
                $this.on('ifChecked', function(e) {
                    $this.parents('tr').css('background-color', '#EEEEEE');
                });
                $this.on('ifUnchecked', function(e) {
                    $this.parents('tr').removeAttr('style');
                });
            });
            $('#selected-all').on('ifChanged', function(event) {
                var $input = $('.site-table tbody tr td').find('input');
                $input.iCheck(event.currentTarget.checked ? 'check' : 'uncheck');
            });
        });
    </script>
</div>
</body>
</html>