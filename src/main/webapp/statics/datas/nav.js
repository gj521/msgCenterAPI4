var navs = [{
	"title": "短信厂家",
	"icon": "fa-cubes",
	"spread": true,
	"children": [{
		"title": "厂家列表",
		"icon": "&#xe62d;",
		"href": "button.html"
	}, {
		"title": "厂家新增",
		"icon": "&#xe654;",
		"href": "form.html"
	}]
}, {
	"title": "短信模版",
	"icon": "fa-cogs",
	"spread": false,
	"children": [{
		"title": "模版列表",
		"icon": "&#xe62d;",
		"href": "template/index"
	}, {
		"title": "模版新增",
		"icon": "&#xe654;",
		"href": "template/index"
	}]
}, {
	"title": "短信日志",
	"icon": "&#xe63c;",
	"spread": false,
	"children": [{
		"title": "日志查询",
		"icon": "&#xe615;",
		"href": "logs/index"
	}]
}];